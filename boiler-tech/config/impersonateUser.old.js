var User = require('./../../this//models/User');
var passport = require('passport');

/*
sudo code
  The point of this script is to do the old switch-a-roo. While being constantly aware of who is switched with who...
  
=======================================================================================================

        We are going to change the user with the an impersonated user. This impersonated user will be an admin, webmaster, 
    or someone with the 'impersonate' key.

        We control this from a different script. This program using the: "./app.js" file for routing.
    The route will use this middle-ware:
    //    authConf.AllowedUsers('admin',['impersonate']); //routing middle ware. check: ./config/authorization.js
  
          
    So, we have a private array to hold many instances of:
    'obj{
          user 
          && impersonator 
          && and other stuff we think find important
        };'
    
*/

var allTheImpersonations = []; //private

var Impersonation = function(userId, reqUserId) {
    /*
        We want the user and the user requesting the switch.
    */
    var self = this;
    self.userId = userId;
    self.reqUserId = reqUserId; //user requesting the switch.
    self.timer = 1000 * 60 * 30; //30 minutes

    /*
        This object manages placing/removing itself from allTheImpersonations.
        It will remove itself when the time runs out.
        If you call resetTimer, it will reset the timer.
        init is the "Constructor"
    */

    self.startTimer = function() {
        console.log('starting time bomb');
        return setTimeout(function() {
            endTheImpersonation(self.reqUserId);
        }, self.timer);
    }

    self.timeBomb = self.startTimer();


    self.resetTimer = function() {
        clearTimeout(self.timeBomb);
        self.startTimer();
    };

    self.placeOnShelf = function() {
        console.log("placeOnShelf")
        allTheImpersonations.push(self);
    };

    (self.init = function() {
        console.log('created');
        self.placeOnShelf();
    })();
};




/*
=========================================================================================
=========================================================================================
============       Helper Methods              ==========================================
=========================================================================================


    Method that finds our impresonation obj in 
    Params are: impersonation, index
    Example:
    findOne(reqUserId,function(obj,i){
      //do stuff
    }
*/
var findOne = function(reqUserId, callback) {
    for (var i = 0; i < allTheImpersonations.length; i++) {
        try {
            if (allTheImpersonations[i].reqUserId == reqUserId) {
                callback(allTheImpersonations[i], i);
            }
        }
        catch (ex) {
            //we can just remove anything that shouldn't be here. I will install logger later.
            allTheImpersonations.splice(i, 1);
        }

    }
};

var endTheImpersonation = function(reqUserId, req, res) {
    var recordIndex = null;
    findOne(reqUserId, function(obj, i) {
        recordIndex = i;
    });

    if(req){
        User.findOne({ _id: reqUserId }, function(err, existingUser) {

            if(err){
                return;
            }
            req.logout();
            req.logIn(existingUser, function(err) {
                if (err) {
                    return;
                }
    
                allTheImpersonations.splice(recordIndex, 1);
                passport.session();
                req.session.user = null;
                res.locals.impersonationInProgress = null;
                return res.redirect('/app/index');
            });

        });
    } else {
        allTheImpersonations.splice(recordIndex, 1);
    }
    
};


var restartTimer = function(reqUserId) {
    findOne(reqUserId, function(obj, i) {
        obj.resetTimer();
    });
};
var processImpersonation = function(userId, reqUserId, req, res, next) {
    //get userid from mongo..now it time for the old switch-a-roo
    //copy reqUser, log-in as user, paste reqUser in new object call 'impersonationInProgress'. ta-da 
    User.findOne({ _id: userId }, function(err, existingUser) {
        findOne(reqUserId, function(obj, i) {
            //now it's finally time for the old switch-a-roo
            var temp = req.user;
            req.logout();
            req.logIn(existingUser, function(err) {
                if (err) {
                    return next(err);
                }
                
                if(!req.session.user)   
                req.session.user = temp;
                
                passport.session();
                
                res.locals.impersonationInProgress = reqUserId;

                return next();
                
            });
        });
    });

};

/*

=========================================================================================
=========================================================================================
============       Middle-ware                 ==========================================
=========================================================================================
      Dependency: some kinda of auth that creates user object on req... req.user
*/

exports.check = function(req, res, next) {
    if (!req.user) return next();
    
    var requestInProgress = false,
        record = null;
    
    
    for (var i = 0; i < allTheImpersonations.length; i++) {
        if (allTheImpersonations[i].reqUserId == req.user.id) {
            requestInProgress = true;
            record = allTheImpersonations[i];
        }
        else if (req.session.user && req.session.user._id == allTheImpersonations[i].reqUserId) {
            requestInProgress = true;
            record = allTheImpersonations[i];
        }
    
    }
    
    if (requestInProgress) {
        processImpersonation(record.userId, record.reqUserId, req, res, next);
    }
    else {
        return next();
    }

}

/*
    expose a function for starting an impersonation
*/
exports.start = function(userId, reqUserId, req, res) {
    endTheImpersonation(reqUserId); //user is only allowed one impersonation at a time.
    var ImpersonationSession = new Impersonation(userId, reqUserId); //This activates the obj, addes it to the array, start the timer..
}

/*
    expose a function for ending an impersonation
*/
exports.end = function(reqUserId, req, res) {
    endTheImpersonation(reqUserId, req, res);
}

/*
    expose a function for ending an impersonation
*/
exports.resetTimer = function(reqUserId) {
    restartTimer(reqUserId);
}
