var passportConf = require('./passport');


exports.AllowedUsers = function(role, allowedkeys) {
  return [
    passportConf.isAuthenticated,
    function(req, res, next) {
      if (req.user && (checkRole(req.user.role, role) || checkkeys(req.user.keys, allowedkeys)))
        next();
      else
        res.send(401, 'Unauthorized');
    }
  ];
};

var checkkeys = function(userPermisions, allowedkeys) {
  //early return, is no allowed keys, return false
  if(typeof(allowedkeys) == 'undefined' || allowedkeys == null || allowedkeys.length == 0){
    return false;
  }
  
  var userPass = false;

  firstLoop:
    for (var i = 0; i < userPermisions.length; i++) {
      for (var j = 0; j < allowedkeys.length; j++) {
        if (allowedkeys[j] == userPermisions[i]) {
          userPass = true;
          break firstLoop;
        }
      }
    }

  return userPass;
}

var checkRole = function(userRole, allowedRole) {
  //early returns. webmaster can enter anything. Also if no value was given, we assume that we will only filter by key
  if (userRole == 'webmaster') {
    return true;
  }
  else if (typeof(allowedRole) === 'undefined' || allowedRole === '' || allowedRole === null) {
    return false;
  }

  var userPass = false;
  switch (userRole) {
    case 'admin':
      if (allowedRole == 'admin' || allowedRole == 'manager' || allowedRole == 'worker') {
        userPass = true;
      }
      break;
    case 'manager':
      if (allowedRole == 'manager' || allowedRole == 'worker') {
        userPass = true;
      }
      break;
    case 'worker':
      if (allowedRole == 'worker') {
        userPass = true;
      }
      break;
  }

  return userPass;
}