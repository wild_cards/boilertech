var User = require('./../../this/models/User');
var Impersonation = require('./../../boiler-tech/models/Impersonation');
var passport = require('passport');


//startAnImpersonation
var endTheImpersonation = function(reqUserId, req, res) {
    var recordIndex = null;
    findOne(reqUserId, function(obj, i) {
        recordIndex = i;
    });

    if (req) {
        User.findOne({
            _id: reqUserId
        }, function(err, existingUser) {

            if (err) {
                return;
            }
            req.logout();
            req.logIn(existingUser, function(err) {
                if (err) {
                    return;
                }

                allTheImpersonations.splice(recordIndex, 1);
                passport.session();
                req.session.user = null;
                res.locals.impersonationInProgress = null;
                return res.redirect('/app/index');
            });

        });
    }
    else {
        allTheImpersonations.splice(recordIndex, 1);
    }

};


var restartTimer = function(reqUserId) {
    // findOne(reqUserId, function(obj, i) {
    //     obj.resetTimer();
    // });
};

/*

=========================================================================================
=========================================================================================
============       Middle-ware                 ==========================================
=========================================================================================
      Dependency: some kinda of auth that creates user object on req... req.user
*/

exports.check = function(req, res, next) {
    if(Impersonation.checkForActiveImpersonation(req, res, next)){
        
    };
    // if (!req.user) return next();

    // var requestInProgress = false,
    //     record = null;


    // for (var i = 0; i < allTheImpersonations.length; i++) {
    //     if (allTheImpersonations[i].reqUserId == req.user.id) {
    //         requestInProgress = true;
    //         record = allTheImpersonations[i];
    //     }
    //     else if (req.session.user && req.session.user._id == allTheImpersonations[i].reqUserId) {
    //         requestInProgress = true;
    //         record = allTheImpersonations[i];
    //     }

    // }

    // if (requestInProgress) {
    //     processImpersonation(record.userId, record.reqUserId, req, res, next);
    // }
    // else {
    //     return next();
    // }

}

/*
    expose a function for starting an impersonation
*/
exports.start = function(userBeingImpersonateId, userRequestingImpersonationId, req, res) {
    Impersonation.startAnImpersonation(userBeingImpersonateId, userRequestingImpersonationId);
    // console.log('start');
    // var blocking;
    // // endTheImpersonation(reqUserId); //user is only allowed one impersonation at a time.
    // // var ImpersonationSession = new Impersonation(userId, reqUserId); //This activates the obj, addes it to the array, start the timer..

    // //get userid from mongo..now it time for the old switch-a-roo
    // //copy reqUser, log-in as user, paste reqUser in new object call 'impersonationInProgress'. ta-da 
    // User.findOne({
    //     _id: userId
    // }, function(err, existingUser) {
    //     //findOne(reqUserId, function(obj, i) {
    //         //now it's finally time for the old switch-a-roo
    //         var temp = req.user;
    //         blocking = req.logout();
    //         blocking = req.logIn(existingUser, function(err) {
    //             console.log('new user logged in');
    //             if (err) {
    //                 return;
    //             }

    //             req.session.user = temp;
                
    //             console.log(req.user);
    //             passport.session();
    //             res.locals.user = req.user;
    //             //res.locals.impersonationInProgress = reqUserId;

    //             return;

    //         });
    //     //});
    // });


}

/*
    expose a function for ending an impersonation
*/
exports.end = function(reqUserId, req, res) {
    endTheImpersonation(reqUserId, req, res);
}

/*
    expose a function for ending an impersonation
*/
exports.resetTimer = function(reqUserId) {
    restartTimer(reqUserId);
}
