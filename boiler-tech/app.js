//nodemon boiler-tech/app.js --ignore boiler-tech/app.js --ignore boiler-tech/views --watch this --watch boiler-tech -e js,jade,css
//boiler-tech file engine
var fs_engine = require('./fs_engine/build'),
  bt_load = true;

var bt_blocking = bt_load ? fs_engine.init() : typeof(undefined);

/**
 * Module dependencies.
 */
var express = require('express');
var multer = require('multer');
var cookieParser = require('cookie-parser');
var compress = require('compression');
var favicon = require('serve-favicon');
var session = require('express-session');
var bodyParser = require('body-parser');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var lusca = require('lusca');
var methodOverride = require('method-override');

var _ = require('lodash');
var MongoStore = require('connect-mongo/es5')(session);
var flash = require('express-flash');
var path = require('path');
var mongoose = require('mongoose');
var passport = require('passport');
var expressValidator = require('express-validator');
var sass = require('node-sass-middleware');
var upload = multer({
  dest: null
});

var App_Setting = require('./../this/models/App_Setting');
//var inpersonateUser = require('./config/impersonateUser');
var Impersonation = require('./../this/models/Impersonation');
var User = require('./../this/models/User');
// var Company = require('./../this/models/Company');
// Company.save();
/**
 * Create Express server.
 */
var http = require('http');
var socketio = require('socket.io');
var app = express();
var server = http.createServer(app);
var io = socketio.listen(server);


/**
 * API keys and Passport configuration.
 */
var secrets = require('./../this/infrastructure/secrets');
var passportConf = require('./config/passport');
var authConf = require('./config/authorization');



/**
 * Connect to MongoDB.
 */
mongoose.connect(secrets.db);
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views/'));
app.set('view engine', 'jade');
app.use(compress());
app.use(sass({
  src: path.join(__dirname, '../public'),
  dest: path.join(__dirname, '../public'),
  debug: true,
  sourceMap: true,
  outputStyle: 'expanded'
}));
app.use(logger('dev'));
app.use(favicon(path.join(__dirname,'..' , 'public', 'favicon2.ico')));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(expressValidator());
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: secrets.sessionSecret,
  store: new MongoStore({
    url: secrets.db,
    autoReconnect: true
  })
}));
app.use(passport.initialize());

app.use(passport.session());


app.use(flash());
app.use(function(req, res, next) {
  if (req.path === '/upload' || req.path === '/account/profile') {
    next();
  } else {
    lusca.csrf({
    csrf: true,
    xframe: 'SAMEORIGIN',
    xssProtection: true
  })(req, res, next);
  }
});
// app.use(
  
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   ///         SHOULD  BE   REPLACED ASAP. This works for time issues. You want to build the multipart/form data req//////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////////////////////////////////////////////
//   //if(req.body.avatar || req.body.uploads){

//   //}
// );

app.use(function(req, res, next) {
  res.locals.user = req.user;
  res.locals.impersonationInProgress = req.session.impersonationInProgress;
  res.locals._ = _;
  app.locals.stripePubKey = secrets.stripeOptions.stripePubKey;
  //next();
  App_Setting.getOne(function(err, setting) {

    res.locals.appSetting = setting;
    next();
  });
});

// else {
//   console.log('boiler-tech: No impersonation during request.');
//   res.locals.user = req.user;

// }
// return next();
// //console.log(res.locals.user);

// });

app.use(function(req, res, next) {
  if (/api/i.test(req.path)) {
    req.session.returnTo = req.path;
  }
  next();
});
app.use(express.static(path.join(__dirname, '../public'), {
  maxAge: 31557600000
}));


// // //putting realtime first.
// var realtimeController = require("./../this/projects/app/notifications/server/realtime")(io);
// app.use('/', passportConf.isAuthenticated, realtimeController);

/*
/////////////////////////////////////////////////////////////////////////
This section is generated. Don't save code here
/////////////////////////////////////////////////////////////////////////
*/
//!l0ad!R0ut3s//Do not Edit
var userController = require("./../this/projects/app/account/server/account-controller");
app.get('/login', userController.getLogin);
app.post('/login', userController.postLogin);
app.get('/logout', userController.logout);
app.get('/loggedout', userController.loggedout);
app.get('/forgot', userController.getForgot);
app.post('/forgot', userController.postForgot);
app.get('/reset/:token', userController.getReset);
app.post('/reset/:token', userController.postReset);
app.get('/signup', userController.getSignup);
app.post('/signup', userController.postSignup);
app.get('/account', passportConf.isAuthenticated, userController.getAccount);
app.post('/account/profile', upload.single('avatar'), passportConf.isAuthenticated, userController.postUpdateProfile);
app.post('/account/password', passportConf.isAuthenticated, userController.postUpdatePassword);
app.post('/account/delete', passportConf.isAuthenticated, userController.postDeleteAccount);
app.get('/account/unlink/:provider', passportConf.isAuthenticated, userController.getOauthUnlink);
app.get('/api/v1/users/getAll', passportConf.isAuthenticated, userController.getAllUsersApi);
app.get('/app/images', userController.getImage);
app.get('/auth/instagram', passport.authenticate('instagram'));
app.get('/auth/instagram/callback', passport.authenticate('instagram', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/'); });
app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email', 'user_location'] }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/'); });
app.get('/auth/github', passport.authenticate('github'));
app.get('/auth/github/callback', passport.authenticate('github', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/'); });
app.get('/auth/google', passport.authenticate('google', { scope: 'profile email' }));
app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/'); });
app.get('/auth/twitter', passport.authenticate('twitter'));
app.get('/auth/twitter/callback', passport.authenticate('twitter', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/'); });
app.get('/auth/linkedin', passport.authenticate('linkedin', { state: 'SOME STATE' }));
app.get('/auth/linkedin/callback', passport.authenticate('linkedin', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/'); });
app.get('/auth/foursquare', passport.authorize('foursquare'));
app.get('/auth/foursquare/callback', passport.authorize('foursquare', { failureRedirect: '/api' }), function(req, res) { res.redirect('/api/foursquare'); });
app.get('/auth/tumblr', passport.authorize('tumblr'));
app.get('/auth/tumblr/callback', passport.authorize('tumblr', { failureRedirect: '/api' }), function(req, res) { res.redirect('/api/tumblr'); });
app.get('/auth/venmo', passport.authorize('venmo', { scope: 'make_payments access_profile access_balance access_email access_phone' }));
app.get('/auth/venmo/callback', passport.authorize('venmo', { failureRedirect: '/api' }), function(req, res) { res.redirect('/api/venmo');});
app.get('/auth/steam', passport.authorize('openid', { state: 'SOME STATE' }));
app.get('/auth/steam/callback', passport.authorize('openid', { failureRedirect: '/login' }), function(req, res) { res.redirect(req.session.returnTo || '/');});
var appAdmin_Controller = require("./../this/projects/app/admin/server/admin-controller");
app.get('/app/company_info', appAdmin_Controller.getCompanyInfo);
app.post('/app/company_info', appAdmin_Controller.postCompanyInfo);
app.get('/app/manage_users', appAdmin_Controller.getManageUsers);
app.get('/app/manage_user', appAdmin_Controller.getManageUser);
app.post('/app/manage_user', appAdmin_Controller.postManageUser);
var dashboardController = require("./../this/projects/app/dashboard/server/dashboard-controller");
app.get('/app/dashboard', dashboardController.getDashboard);
var realtimeController = require("./../this/projects/app/notifications/server/realtime")(io);
app.use('/', passportConf.isAuthenticated, realtimeController);
var AppPaymentControrller = require('./../this/projects/app/payment/server/payment-controller');
var appBillingController = require('./../this/projects/app/payment/server/billingController');
app.get('/app/payment_center', passportConf.isAuthenticated, AppPaymentControrller.getPaymentHome);
app.get('/app/payment_issue', passportConf.isAuthenticated, AppPaymentControrller.getPaymentIssue);
app.post('/app/payment_issue', passportConf.isAuthenticated, AppPaymentControrller.postPaymentIssue);
app.get('/app/payment_view_invoice', passportConf.isAuthenticated, AppPaymentControrller.getPaymentInvoiceViewable);
app.get('/app/payment_view_reciept', passportConf.isAuthenticated, AppPaymentControrller.getRecieptViewable);
app.post('/app/charge', passportConf.isAuthenticated, AppPaymentControrller.getCharge);
app.get('/dashboard', passportConf.isAuthenticated, appBillingController.getDefault);
app.get('/billing', passportConf.isAuthenticated, appBillingController.getBilling);
app.post('/app/billing', passportConf.isAuthenticated, appBillingController.postBilling);
app.post('/app/plan', passportConf.isAuthenticated, appBillingController.postPlan);
var mainFrontPageController = require("./../this/projects/main/frontpage/server/controller");
app.get('/', mainFrontPageController.index);
var marketingLead_Controller = require("./../this/projects/marketing/leads/server/marketing_lead_controller");
app.get('/marketing/site-leads', marketingLead_Controller.getSiteLeads);
app.get('/marketing/site-lead', marketingLead_Controller.getSiteLead);
var siteBuilder_Controller = require("./../this/projects/site/builder/server/thisSiteBuilderController");
app.get('/site/builder', siteBuilder_Controller.getSiteBuilder);
app.get('/site/get-site', siteBuilder_Controller.getSite);
app.post('/site/change-colors', siteBuilder_Controller.postChangeColors);
app.post('/site/manage-bio', siteBuilder_Controller.postManageBio);
app.post('/site/manage-reviews', siteBuilder_Controller.postManageReviews);
app.post('/site/manage-services', siteBuilder_Controller.postManageServices);
var siteViewer_Controller = require("./../this/projects/site/viewer/server/thisSiteViewerController");
app.get('/sites/:id/', siteViewer_Controller.getSiteViewerHome);
app.get('/sites/:id/home', siteViewer_Controller.getSiteViewerHome);
app.get('/sites/:id/contact', siteViewer_Controller.getSiteViewerContact);
app.get('/sites/:id/projects', siteViewer_Controller.getSiteViewerProjects);
app.get('/sites/:id/pictures', siteViewer_Controller.getSiteViewerPictures);
app.get('/sites/:id/reviews', siteViewer_Controller.getSiteViewerReviews);
app.get('/sites/:id/services', siteViewer_Controller.getSiteViewerServices);
app.get('/sites/:id/thankyou', siteViewer_Controller.getSiteViewerThankYou);
app.post('/sites/:id/lead', siteViewer_Controller.postSiteLead)
var apiController = require('./projects/assets/example-api/server/api-controller');
app.get('/root/api', apiController.getApi);
app.get('/api/lastfm', apiController.getLastfm);
app.get('/api/nyt', apiController.getNewYorkTimes);
app.get('/api/aviary', apiController.getAviary);
app.get('/api/steam', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getSteam);
app.get('/api/stripe', apiController.getStripe);
app.post('/api/stripe', apiController.postStripe);
app.get('/api/scraping', apiController.getScraping);
app.get('/api/twilio', apiController.getTwilio);
app.post('/api/twilio', apiController.postTwilio);
app.get('/api/clockwork', apiController.getClockwork);
app.post('/api/clockwork', apiController.postClockwork);
app.get('/api/foursquare', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getFoursquare);
app.get('/api/tumblr', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getTumblr);
app.get('/api/facebook', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getFacebook);
app.get('/api/github', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getGithub);
app.get('/api/twitter', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getTwitter);
app.post('/api/twitter', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.postTwitter);
app.get('/api/venmo', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getVenmo);
app.post('/api/venmo', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.postVenmo);
app.get('/api/linkedin', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getLinkedin);
app.get('/api/instagram', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getInstagram);
app.get('/api/yahoo', apiController.getYahoo);
app.get('/api/paypal', apiController.getPayPal);
app.get('/api/paypal/success', apiController.getPayPalSuccess);
app.get('/api/paypal/cancel', apiController.getPayPalCancel);
app.get('/api/lob', apiController.getLob);
app.get('/api/bitgo', apiController.getBitGo);
app.post('/api/bitgo', apiController.postBitGo);
var exampleAppController = require("./projects/assets/example-app/server/controller");
app.get('/root/exampleapp/alerts', authConf.AllowedUsers('admin',['demo']), exampleAppController.getAlerts);
app.get('/root/exampleapp/analytics', exampleAppController.getAnalytics);
app.get('/root/root/exampleapp/app_plans', exampleAppController.getAppPlans);
app.get('/root/exampleapp/buttons', exampleAppController.getButtons);
app.get('/root/exampleapp/blog', exampleAppController.getBlog);
app.get('/root/exampleapp/blog_details', exampleAppController.getBlogDetails);
app.get('/root/exampleapp/calendar', exampleAppController.getCalendar);
app.get('/root/exampleapp/chartist', exampleAppController.getChartist);
app.get('/root/exampleapp/chartjs', exampleAppController.getChartjs);
app.get('/root/exampleapp/chat_view', exampleAppController.getChatView);
app.get('/root/exampleapp/code_editor', exampleAppController.getCodeEditor);
app.get('/root/exampleapp/contacts', exampleAppController.getContacts);
app.get('/root/exampleapp/components', exampleAppController.getComponents);
app.get('/root/exampleapp/datatables', exampleAppController.getDatatables);
app.get('/root/exampleapp/draggable', exampleAppController.getDraggable);
app.get('/root/exampleapp/email_template', exampleAppController.getEmailTemplate);
app.get('/root/exampleapp/empty_starter', exampleAppController.getEmptyStarter);
app.get('/root/exampleapp/error_one', exampleAppController.getErrorOne);
app.get('/root/exampleapp/error_two', exampleAppController.getErrorTwo);
app.get('/root/exampleapp/faq', exampleAppController.getFaq);
app.get('/root/exampleapp/file_manager', exampleAppController.getFileManager);
app.get('/root/exampleapp/flot', exampleAppController.getFlot);
app.get('/root/exampleapp/footable', exampleAppController.getFootable);
app.get('/root/exampleapp/forms_elements', exampleAppController.getFormsElements);
app.get('/root/exampleapp/forms_extended', exampleAppController.getFormsExtended);
app.get('/root/exampleapp/forum', exampleAppController.getForum);
app.get('/root/exampleapp/forum_details', exampleAppController.getForumDetails);
app.get('/root/exampleapp/gallery', exampleAppController.getGallery);
app.get('/root/exampleapp/grid_system', exampleAppController.getGridSystem);
app.get('/root/exampleapp/icons', exampleAppController.getIcons);
app.get('/root/exampleapp/index', exampleAppController.getIndex);
app.get('/root/exampleapp/inline', exampleAppController.getInline);
app.get('/root/exampleapp/invoice', exampleAppController.getInvoice);
app.get('/root/exampleapp/landing_page', exampleAppController.getLandingPage);
app.get('/root/exampleapp/lock', exampleAppController.getLock);
app.get('/root/exampleapp/login', exampleAppController.getLogin);
app.get('/root/exampleapp/mailbox', exampleAppController.getMailbox);
app.get('/root/exampleapp/mailbox_compose', exampleAppController.getMailboxCompose);
app.get('/root/exampleapp/mailbox_view', exampleAppController.getMailboxView);
app.get('/root/exampleapp/modals', exampleAppController.getModals);
app.get('/root/exampleapp/nestable_list', exampleAppController.getNestableList);
app.get('/root/exampleapp/notes', exampleAppController.getNotes);
app.get('/root/exampleapp/options', exampleAppController.getOptions);
app.get('/root/exampleapp/overview', exampleAppController.getOverview);
app.get('/root/exampleapp/panels', exampleAppController.getPanels);
app.get('/root/exampleapp/profile', exampleAppController.getProfile);
app.get('/root/exampleapp/project', exampleAppController.getProject);
app.get('/root/exampleapp/projects', exampleAppController.getProjects);
app.get('/root/exampleapp/register', exampleAppController.getRegister);
app.get('/root/exampleapp/search', exampleAppController.getSearch);
app.get('/root/exampleapp/social_board', exampleAppController.getSocialBoard);
app.get('/root/exampleapp/tables_design', exampleAppController.getTablesDesign);
app.get('/root/exampleapp/text_editor', exampleAppController.getTextEditor);
app.get('/root/exampleapp/timeline', exampleAppController.getTimeline);
app.get('/root/exampleapp/transition_one', exampleAppController.getTransitionOne);
app.get('/root/exampleapp/transition_two', exampleAppController.getTransitionTwo);
app.get('/root/exampleapp/transition_three', exampleAppController.getTransitionThree);
app.get('/root/exampleapp/transition_four', exampleAppController.getTransitionFour);
app.get('/root/exampleapp/transition_five', exampleAppController.getTransitionFive);
app.get('/root/exampleapp/tour', exampleAppController.getTour);
app.get('/root/exampleapp/typography', exampleAppController.getTypography);
app.get('/root/exampleapp/validation', exampleAppController.getValidation);
app.get('/root/exampleapp/widgets', exampleAppController.getWidgets);
app.get('/root/exampleapp/wizard', exampleAppController.getWizard);
var rootIndexController = require("./projects/root/dashboard/server/controller");
app.get('/root/dashboard', passportConf.isAuthenticated, rootIndexController.index);
var rootManageCompaniesController = require("./projects/root/manage_companies/server/manage_companies-controller.js");
app.get('/root/manage_companies',rootManageCompaniesController.getManageCompanies);
app.get('/root/manage_company',rootManageCompaniesController.getManageCompany);
app.get('/root/add_user_to_company',rootManageCompaniesController.getAddUserToCompany);
app.get('/root/remove_user_from_company',rootManageCompaniesController.getRemoveUserFromCompany);
app.post('/root/remove_user_from_company',rootManageCompaniesController.postRemoveUserFromCompany);
app.get('/root/create_company',rootManageCompaniesController.getCreateCompany);
app.post('/root/create_company',rootManageCompaniesController.postCreateCompany);
var rootManageKeysController = require("./projects/root/manage_keys/server/manage-keys-controller");
app.get('/root/manage_keys', authConf.AllowedUsers('webmaster'),rootManageKeysController.getWebmasterManagekeys);
app.get('/root/create_key', authConf.AllowedUsers('webmaster'),rootManageKeysController.getNewkey);
app.post('/root/create_key', authConf.AllowedUsers('webmaster'),rootManageKeysController.postNewkey);
app.get('/root/key_is_assignable/:key', authConf.AllowedUsers('webmaster'),rootManageKeysController.getAssignable);
app.post('/root/key_is_assignable', authConf.AllowedUsers('webmaster'),rootManageKeysController.postAssignable);
app.get('/root/key_purpose/:key', authConf.AllowedUsers('webmaster'),rootManageKeysController.getPurpose);
app.post('/root/key_purpose', authConf.AllowedUsers('webmaster'),rootManageKeysController.postPurpose);
var rootManageUsersController = require("./projects/root/manage_users/server/manage_users-controller");
app.get('/root/manage_users', authConf.AllowedUsers('admin'), rootManageUsersController.getAdminManageUsers);
app.get('/root/manage_user/:id', authConf.AllowedUsers('admin'), rootManageUsersController.getAdminManageUser);
app.post('/root/manage_user', authConf.AllowedUsers('admin'), rootManageUsersController.postAdminManageUser)
app.get('/root/lock_out/:id', authConf.AllowedUsers('admin'), rootManageUsersController.getLockOut);
app.post('/root/lock_out', authConf.AllowedUsers('admin'), rootManageUsersController.postLockOut);
app.get('/root/make_webmaster/:id', authConf.AllowedUsers('webmaster'), rootManageUsersController.getWebmasterMakeWebmaster);
app.post('/root/make_webmaster', authConf.AllowedUsers('webmaster'), rootManageUsersController.postWebmasterMakeWebmaster);
app.get('/root/start_impersonation/:userId', authConf.AllowedUsers('admin', ['impersonation']), rootManageUsersController.getStartImpersonation);
app.get('/root/end_impersonation/:id', authConf.AllowedUsers('admin',['impersonation']), rootManageUsersController.getEndImpersonation);
var notificationsRootController = require('./projects/root/notifications/server/notificationsRootController');
app.get('/root/sendNotifications', authConf.AllowedUsers('admin'), notificationsRootController.getSendNotifications);
app.post('/root/sendNotifications', authConf.AllowedUsers('admin'), notificationsRootController.postSendNotifications);
var rootInvoicesController = require("./projects/root/payment/server/rootInvoicesController");
app.get('/root/payments', rootInvoicesController.getManageInvoices);
app.get('/root/payment', rootInvoicesController.getManageInvoice);
app.get('/root/create_invoice', rootInvoicesController.getCreateInvoice);
app.post('/root/create_invoice', rootInvoicesController.postCreateInvoice);
app.get('/root/end_invoice', rootInvoicesController.getEndInvoice);
app.post('/root/end_invoice', rootInvoicesController.postEndInvoice);
app.post('/root/change_invoice', rootInvoicesController.postChangeInvoice);
app.post('/root/invoice_issues', rootInvoicesController.postInvoiceIssues);
var rootSettingController = require("./projects/root/settings/server/rootSetting-controller");
app.get('/root/settings',rootSettingController.getSettings);
app.post('/root/settings',rootSettingController.postSettings);
//!l0ad!R0ut3s//Do not Edit
/*
/////////////////////////////////////////////////////////////////////////
End Of: This section is generated. Don't save code here
/////////////////////////////////////////////////////////////////////////
*/


/**
 * Error Handler.
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
app.listen(app.get('port'), function() {
  console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;
