var Company = require('./../../../../../this/models/Company');
var Invoice = require('./../../../../../this/models/Invoice');
var InvoiceIssue = require('./../../../../../this/models/Invoice_Issue');
var Notifications = require('./../../../../../this/models/Notification');


exports.getManageInvoices = function(req, res, next) {
  Company.getAll(function(err, companies) {
    res.render('boiler-tech/projects/root/payment/view-all', {
      title: 'Manage Invoices',
      companies: companies
        // permissions: returningPermissions,
        // webmaster_permissions: returningWebmasterPermissions,
        // selectedRole: selectedRole

    });

  });
};

exports.getManageInvoice = function(req, res, next) {
  Company.getOne_advance(req.query.id, function(err, company) {
    Invoice.getOne(company.currentInvoiceId, function(err2, invoice) {


      if (invoice) {
        var invoiceItems = [];
        for (var i = 0; i < invoice.items.length; i++) {
          invoiceItems.push({
            name: invoice.items[i].name,
            price: invoice.items[i].price,
            quantity: invoice.items[i].quantity,
            total: invoice.items[i].total
          });
        }
        InvoiceIssue.getOne(invoice.invoiceIssueId, function(err3, invoiceIssue) {
          res.render('boiler-tech/projects/root/payment/view', {
            title: 'Manage Invoice',
            company: company,
            invoice: invoice,
            invoiceIssue: invoiceIssue,
            invoiceItems: JSON.stringify(invoiceItems)
              // permissions: returningPermissions,
              // webmaster_permissions: returningWebmasterPermissions,
              // selectedRole: selectedRole

          });

        });
      }
      else {
        res.render('boiler-tech/projects/root/payment/view', {
          title: 'Manage Invoice',
          company: company,
          invoice: invoice,
          invoiceItems: JSON.stringify(invoiceItems)
            // permissions: returningPermissions,
            // webmaster_permissions: returningWebmasterPermissions,
            // selectedRole: selectedRole

        });
      };

    });
  });
};

exports.getCreateInvoice = function(req, res, next) {
  Company.getOne_advance(req.query.id, function(err, company) {
    res.render('boiler-tech/projects/root/payment/create', {
      title: 'Create Invoice',
      company: company
        // permissions: returningPermissions,
        // webmaster_permissions: returningWebmasterPermissions,
        // selectedRole: selectedRole

    });
  });
};

exports.postCreateInvoice = function(req, res, next) {
  Invoice.create(req, function(err) {
    if (err) {
      return next(err);
    }
    else {
      req.flash('success', {
        msg: 'Invoice was successfully created.'
      });

      return res.redirect('/root/payment?id=' + req.body.companyId);
    }
  });
};

exports.getEndInvoice = function(req, res, next) {
  Company.getOne_advance(req.query.id, function(err, company) {
    res.render('boiler-tech/projects/root/payment/end', {
      title: 'End Invoice',
      company: company
        // permissions: returningPermissions,
        // webmaster_permissions: returningWebmasterPermissions,
        // selectedRole: selectedRole
    });
  });
};

exports.postEndInvoice = function(req, res, next) {
  Invoice.closeInvoice(req, function(err) {
    if (err) {
      return next(err);
    }
    else {
      req.flash('success', {
        msg: 'Invoice was successfully closed.'
      });

      return res.redirect('/root/payment?id=' + req.body.companyId);
    };
  });
};

exports.postChangeInvoice = function(req, res, next) {
  Invoice.changeInvoice(req, function(err) {
    if (err) {
      console.log(err);
      return next(err);
    }
    else {
      req.flash('success', {
        msg: 'Invoice was successfully closed.'
      });

      return res.redirect('/root/payment?id=' + req.body.companyId);
    };
  });
};

exports.postInvoiceIssues = function(req, res, next) {
  InvoiceIssue.saveOne_advance(req.body.invoiceIssueId, req.body.companyId,
    req.body.invoiceId, req.user.id, req.body.message, req.user.profile.name,
    function(err) {
      if (err) {
        return next(err);
      }
      else {
        req.flash('success', {
          msg: 'Invoice issue response was successfully created.'
        });

        return res.redirect('/root/payment?id=' + req.body.companyId);
      }
    });
};