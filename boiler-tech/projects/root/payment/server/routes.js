var rootInvoicesController = require("~/rootInvoicesController");

app.get('/root/payments', rootInvoicesController.getManageInvoices);

app.get('/root/payment', rootInvoicesController.getManageInvoice);
app.get('/root/create_invoice', rootInvoicesController.getCreateInvoice);
app.post('/root/create_invoice', rootInvoicesController.postCreateInvoice);
app.get('/root/end_invoice', rootInvoicesController.getEndInvoice);
app.post('/root/end_invoice', rootInvoicesController.postEndInvoice);

app.post('/root/change_invoice', rootInvoicesController.postChangeInvoice);
app.post('/root/invoice_issues', rootInvoicesController.postInvoiceIssues);

