var items = [];

var loadItems = function() {
    var tempString = '';
    for (var i = 0; i < items.length; i++) {
        tempString +=
            "<tr>\
                <td>" +
                    items[i].name +
                    "<input name='items[" + i + "].name' type='hidden' value='" + items[i].name + "'/> \
                </td>\
                <td>" +
                    items[i].price +
                    "<input name='items[" + i + "].price' type='hidden' value='" + items[i].price + "'/> \
                    </td>\
                <td>" +
                    items[i].quantity +
                    "<input name='items[" + i + "].quantity' type='hidden' value='" + items[i].quantity + "'/> \
                    </td>\
                <td> \
                    <div class='btn btn-danger' onclick='deleteItem("+i+")'>\
                        Delete\
                    </div>\
                </td>\
            </tr>";
    }

    $('#itemTable').html(tempString);
}

var addItem = function() {
    items.push({
        name: $('#itemName').val(),
        price: $('#itemPrice').val(),
        quantity: $('#itemQuantity').val()
    })
    loadItems();
    $('#itemName').val('');
    $('#itemPrice').val('');
    $('#itemQuantity').val('');
}

var deleteItem = function(id) {
    items.splice(id,1);
    loadItems();
}

$(function() {
    $('#datepicker').datepicker();
    $("#datepicker").on("changeDate", function(event) {
        $("#my_hidden_input").val(
            $("#datepicker").datepicker('getFormattedDate')
        )
    });

    $('#datapicker2').datepicker();
    $('.input-group.date').datepicker({});
    $('.input-daterange').datepicker({});
    
});
