var rootManageCompaniesController = require("~/manage_companies-controller.js");

app.get('/root/manage_companies',rootManageCompaniesController.getManageCompanies);
app.get('/root/manage_company',rootManageCompaniesController.getManageCompany);
app.get('/root/add_user_to_company',rootManageCompaniesController.getAddUserToCompany);
app.get('/root/remove_user_from_company',rootManageCompaniesController.getRemoveUserFromCompany);
app.post('/root/remove_user_from_company',rootManageCompaniesController.postRemoveUserFromCompany);

app.get('/root/create_company',rootManageCompaniesController.getCreateCompany);

app.post('/root/create_company',rootManageCompaniesController.postCreateCompany);


//firstSave