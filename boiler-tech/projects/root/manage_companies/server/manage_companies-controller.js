var Company = require('./../../../../../this/models/Company');
var User = require('./../../../../../this/models/User');


exports.getManageCompanies = function(req, res, next) {
    Company.find(function(err, companies) {
        res.render('boiler-tech/projects/root/manage_companies/view-all', {
            title: 'Manage Companies',
            companies: companies
        });
    });
};

exports.getManageCompany = function(req, res, next) {
    Company.getOne_advance(req.query.id, function(err, company) {
        Company.getAllCompanyUsers(req.query.id, function(err, company_users) {
            Company.getUsersWithNoSelectedCompany(req.query.id, function(err, avaibleUsers) {
                res.render('boiler-tech/projects/root/manage_companies/view', {
                    title: company.company_name,
                    company: company,
                    company_users: company_users,
                    avaibleUsers: avaibleUsers
                });
            });
        });
    });
};

exports.getRemoveUserFromCompany = function(req, res, next) {
    Company.getOne_advance(req.query.companyId, function(err, company) {
        User.findOne({
            _id: req.query.userId
        }, function(err, theUser) {
            res.render('boiler-tech/projects/root/manage_companies/remove_user_from_company', {
                title: 'Remove user from company',
                company: company,
                theUser: theUser
            });
        });
    });
}

exports.getCreateCompany = function(req, res, next) {
    res.render('boiler-tech/projects/root/manage_companies/create', {
        title: 'Create new company'
    });
}

exports.postCreateCompany = function(req, res, next) {
    Company.firstSave(req, function(err) {
        if (err) {
            return next(err);
        }
        else {
            req.flash('success', {
                msg: 'Company called "' + req.body.company_name + '" was successfully created.'
            });
            return res.redirect('/root/manage_companies');
        }
    })
}

exports.getAddUserToCompany = function(req, res, next) {
    User.findOne({
        _id: req.query.userId
    }, function(err, user) {
        if (!user || user.selectedCompanyId != req.body.companyId) {
            req.flash('errors', {
                msg: 'Something went wrong.'
            });
            return res.redirect('/root/manage_company?id=' + req.query.companyId);
        }
        user.selectedCompanyId = req.query.companyId;
        user.save(function(err) {
            if (err) {
                return next(err);
            }
            else {
                req.flash('success', {
                    msg: 'User: ' + user.profile.name + ' was successfully added.'
                });
                return res.redirect('/root/manage_company?id=' + req.query.companyId);
            }
        });
    });
}

///////////////
exports.postRemoveUserFromCompany = function(req, res, next) {
    User.findOne({
        _id: req.body.userId
    }, function(err, user) {
        if (!user || user.selectedCompanyId.toString() != req.body.companyId.toString()) {
            req.flash('errors', {
                msg: 'Something went wrong.'
            });
            return res.redirect('/root/manage_company?id=' + req.body.companyId);
        }
        user.selectedCompanyId = null;
        user.save(function(err) {
            if (err) {
                return next(err);
            }
            else {
                req.flash('success', {
                    msg: 'User: ' + user.profile.name + ' was successfully removed.'
                });
                return res.redirect('/root/manage_company?id=' + req.body.companyId);
            }
        });
    });
}