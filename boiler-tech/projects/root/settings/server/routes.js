var rootSettingController = require("~/rootSetting-controller");

app.get('/root/settings',rootSettingController.getSettings);
app.post('/root/settings',rootSettingController.postSettings);