var App_Setting = require('./../../../../../this/models/App_Setting');

exports.getSettings = function(req, res, next) {
    res.render('boiler-tech/projects/root/settings/setting-index', {
        title: 'Settings'
            //users: users,
            // permissions: returningPermissions,
            // webmaster_permissions: returningWebmasterPermissions,
            // selectedRole: selectedRole
    });
};

exports.postSettings = function(req, res, next){
    App_Setting.saveRecord(req, function(err){
      if (err) {
        req.flash('error', {
          msg: 'Something went wrong.'
        });
      }
      else {
        req.flash('success', {
          msg: 'App settings has been updated.'
        });
      }
      return res.redirect('/root/settings');
    
    });
};