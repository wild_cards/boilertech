var keys = require('./../../../../../this/models/Key');
var Users = require('./../../../../../this/models/User');
var moment = require('moment');
var secrets = require('./../../../../../this/infrastructure/secrets');
var User = require('./../../../../../this/models/User');
var Impersonation = require('./../../../../../this/models/Impersonation');

/*
    This file has two roles responsibilities:
        1:Admin
        2:Webmaster
        
    The first section is admin.
    
    Business logic for admin "Manage Users" Portal:
      
      1)Admin(s) can assign roles to fellow admin(s) or below. No one can change a webmaster's role.
      2)Admin(s) can assign keys to fellow admin(s) or below. Webmaster can change any users keys.
      3)There are two types of keys.
        a)keys only webmaster(s) can assign
        b)keys only admin(s) can assign. These keys can be assign viewed/assign by webmaster. 
      4)Only webmaster(s) can view/assign another user to the webmaster role.
      5)Admin(s) can lock out any admin(s) or below account.
      6)Webmaster can only remove a fellow webmaster account with a secret code. They can lock out any other user
*/
////////////////////////////////////////////////////////////////////////////
///////   Admin : app/manage_Users
////////////////////////////////////////////////////////////////////////////

exports.getAdminManageUsers = function(req, res) {
  Users.find(function(err, users) {
    res.render('boiler-tech/projects/root/manage_users/admin_manage_users', {
      title: 'Manage Users',
      users: users
    });
  });
};

////////////////////////////////////////////////////////////////////////////
///////   Admin : app/manage_user/:id
////////////////////////////////////////////////////////////////////////////

exports.getAdminManageUser = function(req, res) {
  Users.findOne({
    _id: req.params.id
  }, function(err, theUser) {
    keys.find(function(err, keys) {

      var returningkeys = [],
        returningWebmasterkeys = [],
        selectedRole = 'User'; //default value for select

      switch (theUser.role) {
        case 'webmaster':
          selectedRole = 'Webmaster'
          break;
        case 'admin':
          selectedRole = 'Admin'
          break;
        case 'manager':
          selectedRole = 'Manager'
          break;
        case 'worker':
          selectedRole = 'Worker'
          break;
      }

      //This funtion figures out what checkbox are checked
      var addCheckForkey = function(key) {
        for (var j = 0; j < theUser.keys.length; j++) {
          if (theUser.keys[j] == key) {
            keys[i].check = true;
          }
        }
      }

      //sorting admin and webmaster keys
      for (var i = 0; i < keys.length; i++) {
        if (keys[i].isAssignable) {
          addCheckForkey(keys[i].name);
          returningkeys.push(keys[i]);
        }
        else {
          addCheckForkey(keys[i].name);
          returningWebmasterkeys.push(keys[i]);
        }
      }



      res.render('boiler-tech/projects/root/manage_users/admin_manage_user', {
        title: 'Manage User',
        theUser: theUser,
        keys: returningkeys,
        webmaster_keys: returningWebmasterkeys,
        selectedRole: selectedRole
      });
    });
  });
};

exports.postAdminManageUser = function(req, res, next) {
  Users.findOne({
    _id: req.body.userId
  }, function(err, theUser) {
    var userkeys = [];

    if (theUser.role != 'webmaster') {
      theUser.role = req.body.role;
    }

    //filter and taking all the selected keys
    for (var key in req.body) {
      if (key != '_csrf' && key != 'role' && key != 'userId') {
        userkeys.push(key);
      }
    }
    theUser.keys = userkeys;

    theUser.save(function(err) {
      if (err) {
        req.flash('error', {
          msg: 'Something went wrong.'
        });
      }
      else {
        req.flash('success', {
          msg: 'User has been updated.'
        });
      }
      return res.redirect('/root/manage_user/' + req.body.userId);
    });
  });
};

////////////////////////////////////////////////////////////////////////////
///////   Admin : app/lock_out/:id
////////////////////////////////////////////////////////////////////////////

exports.getLockOut = function(req, res, next) {
  Users.findOne({
    _id: req.params.id
  }, function(err, theUser) {
    res.render('boiler-tech/projects/root/manage_users/admin_lockout', {
      title: 'Manage Users',
      theUser: theUser
    });
  });
}

exports.postLockOut = function(req, res, next) {
  Users.findOne({
    _id: req.body.id
  }, function(err, theUser) {
    if (theUser.role == 'webmaster') {
      if (req.body.removeWebmasterCode == secrets.lock_out_webmaster) {
        theUser.isLockedOut = true;
      }
    }
    else {
      theUser.isLockedOut = true;
    }
    theUser.save(function(err) {
      if (err) {
        return next(err);
      }
      else {
        req.flash('success', {
          msg: 'key was successfully created.'
        });

        return res.redirect('/root/lock_out/' + req.body.id);
      }
    });
  });
}

exports.getWebmasterMakeWebmaster = function(req, res, next) {
  Users.findOne({
    _id: req.params.id
  }, function(err, theUser) {
    res.render('boiler-tech/projects/root/manage_users/admin_make_webmaster', {
      title: 'Make Webmaster',
      theUser: theUser
    });
  });
}


exports.postWebmasterMakeWebmaster = function(req, res, next) {
  Users.findOne({
    _id: req.body.id
  }, function(err, theUser) {

    theUser.role = 'webmaster';

    theUser.save(function(err) {
      if (err) {
        return next(err);
      }
      else {
        req.flash('success', {
          msg: 'The user "' + theUser.email + '" is now a webmaster'
        });

        return res.redirect('/root/manage_user/' + req.body.id);
      }
    });
  });
}



/*
    ==============================================================================
    ==============================================================================
    ==============================================================================
    ==============================================================================
    ==============================================================================
    ==============================================================================
    ==============================================================================
    ==============================================================================
    The second section are function for the webmaster
*/
exports.getWebmasterManagekeys = function(req, res) {
  keys.find().sort({
    'name': 'ascending'
  }).exec(
    function(err, keys) {
      res.render('boiler-tech/projects/root/manage_users/webmaster_manage', {
        title: 'Manage keys',
        keys: keys,
        moment: moment
      });
    });
}

exports.getNewkey = function(req, res) {
  res.render('boiler-tech/projects/root/manage_users/webmaster_create', {
    title: 'Create New key',
    name: '',
    reason: ''
  });
};

exports.postNewkey = function(req, res, next) {
  req.assert('name', '"Name" cannot be blank').notEmpty();
  req.assert('reason', '"Reason for creation" cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/root/create_key/');
  }
  /////////////////////////////////////////////////////////////

  var newkey = new keys({
    name: req.body.name.toLowerCase(),
    isAssignable: req.body.isAssignable
  });

  newkey.orignalPurpose.reason = req.body.reason;
  newkey.orignalPurpose.createdByUserId = req.user.id;
  newkey.orignalPurpose.createdBy = req.user.profile.name;

  if (req.body.isAssignable) {
    newkey.assignableCreatedBy = req.user.profile.name;
    newkey.assignableCreatedByUserId = req.user.id;
    newkey.assignableCreatedOn = Date.now();
  }


  keys.findOne({
    name: req.body.name
  }, function(err, existingkey) {
    if (existingkey) {
      req.flash('errors', {
        msg: 'key with that name already exists.'
      });
      return res.redirect('/root/create_key');
    }
    newkey.save(function(err) {
      if (err) {
        return next(err);
      }
      else {
        req.flash('success', {
          msg: 'key was successfully created.'
        });
        return res.redirect('/root/manage_keys');
      }
    });
  });
};

exports.getAssignable = function(req, res) {
  res.render('boiler-tech/projects/root/manage_users/webmaster_assignable', {
    title: 'front',
    key: req.params.key
  });
};

exports.postAssignable = function(req, res, next) {
  keys.findOne({
    name: req.body.key
  }, function(err, doc) {

    if (err) {
      req.flash('error', {
        msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
      });
      return res.redirect('/root/key_is_assignable');
    }

    doc.isAssignable = true;
    doc.assignableCreatedBy = req.user.profile.name;
    doc.assignableCreatedByUserId = req.user.id;
    doc.assignableCreatedOn = Date.now();

    doc.save(function(err) {
      if (err) {
        req.flash('error', {
          msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
        });
      }
      else {
        req.flash('success', {
          msg: 'Admin can now assign the following key: ' + req.body.key
        });
      }
      return res.redirect('/root/manage_keys')
    });
  });
};


exports.getPurpose = function(req, res) {
  res.render('boiler-tech/projects/root/manage_users/webmaster_purpose', {
    title: 'front',
    key: req.params.key
  });
};

exports.postPurpose = function(req, res, next) {
  req.assert('purpose', '"New purpose for current key" cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);

    return res.redirect('/root/key_purpose/' + req.body.key);
  }
  /////////////////////////////////////////////////////////////

  keys.findOne({
    name: req.body.key
  }, function(err, doc) {

    if (err || doc === null) {
      req.flash('error', {
        msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
      });
      return res.redirect('/root/key_is_assignable');
    }

    doc.newPurpose.push({
      reason: req.body.purpose,
      createdBy: req.user.profile.name,
      createdByUserId: req.user.id,
      createdOn: Date.now()
    });

    doc.save(function(err) {
      if (err) {
        req.flash('error', {
          msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
        });
      }
      else {
        req.flash('success', {
          msg: 'The key called "' + req.body.key + '" has a new purpose. "' + req.body.purpose + '"'
        });
      }
      return res.redirect('/root/manage_keys')
    });
  });
};



exports.getStartImpersonation = function(req, res, next) {
  var blocking = Impersonation.startAnImpersonation(req.user.id.toString(), req.params.userId.toString());


  User.findOne({
    _id: req.params.userId.toString()
  }, function(err, existingUser) {
    //findOne(reqUserId, function(obj, i) {
    //now it's finally time for the old switch-a-roo
    var temp = req.user.id;
    console.log('userId: ' + req.user.id);
    blocking = req.logout();
    req.logIn(existingUser, function(err) {
      console.log('new user logged in:');
      //console.log(existingUser);
      if (err) {
        console.log(err);
      }
      console.log('userId: ' + req.user.id);

      req.session.user = req.user;
      req.session.impersonationInProgress = true;


      res.locals.user = req.user;
      res.locals.impersonationInProgress = true;

      return res.redirect('/app/dashboard');

    });
  });

};

exports.getEndImpersonation = function(req, res, next) {

  Impersonation.endAnImpersonation(req.params.id, req, res, next);
}
