var rootManageUsersController = require("~/manage_users-controller");

app.get('/root/manage_users', authConf.AllowedUsers('admin'), rootManageUsersController.getAdminManageUsers);
app.get('/root/manage_user/:id', authConf.AllowedUsers('admin'), rootManageUsersController.getAdminManageUser);
app.post('/root/manage_user', authConf.AllowedUsers('admin'), rootManageUsersController.postAdminManageUser)
app.get('/root/lock_out/:id', authConf.AllowedUsers('admin'), rootManageUsersController.getLockOut);
app.post('/root/lock_out', authConf.AllowedUsers('admin'), rootManageUsersController.postLockOut);
app.get('/root/make_webmaster/:id', authConf.AllowedUsers('webmaster'), rootManageUsersController.getWebmasterMakeWebmaster);
app.post('/root/make_webmaster', authConf.AllowedUsers('webmaster'), rootManageUsersController.postWebmasterMakeWebmaster);
//app.post('/root/start_impersonation', authConf.AllowedUsers('admin', ['impersonation']), rootManageUsersController.postStartImpersonation);
app.get('/root/start_impersonation/:userId', authConf.AllowedUsers('admin', ['impersonation']), rootManageUsersController.getStartImpersonation);

app.get('/root/end_impersonation/:id', authConf.AllowedUsers('admin',['impersonation']), rootManageUsersController.getEndImpersonation);

