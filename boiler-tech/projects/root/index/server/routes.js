var rootIndexController = require("~/controller");

app.get('/app/index', passportConf.isAuthenticated, rootIndexController.index);
 