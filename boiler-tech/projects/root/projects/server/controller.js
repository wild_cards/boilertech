var Permissions = require('./../../../../../boiler-tech/models/Permission');

exports.getMain = function(req, res, next){
  res.render('projects/main', {
    title: 'Manage the projects',
    name: '',
    reason: ''
  });    
}

exports.getView = function(req, res, next){
  res.render('projects/view', {
    title: 'Manage the projects',
    name: '',
    reason: ''
  });  
}

exports.getNew = function(req, res, next){
 res.render('projects/new', {
    title: 'Manage the projects'
 }); 
}

exports.postNew = function(req, res, next){
    
}

exports.getPermissions = function(req, res, next){
  Permissions.find(function(err, permissions) {
    var permissionsList = [];
      
    permissions.forEach(function(permission){
      if((req.user.role == 'admin' && permission.isAssignable) || req.user.role == 'webmaster'){
        permissionsList.push({
          name: permission.name,
          id: permission.id
        });
      }
    });
    
    res.send(JSON.stringify(permissionsList));
  }); 
}

exports.postNewProject = function(req, res, next){
  
}
