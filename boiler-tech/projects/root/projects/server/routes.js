var rootProjectsController = require("~/controller");

app.get('/app/projects', authConf.AllowedUsers('admin',['development']), rootProjectsController.getMain);
app.get('/app/project/:id', authConf.AllowedUsers('admin',['development']), rootProjectsController.getView);
app.get('/app/project_new', authConf.AllowedUsers('admin',['development']), rootProjectsController.getNew);
app.post('/app/project_new', authConf.AllowedUsers('admin',['development']), rootProjectsController.postNew);
app.get('/api/v1/getPermissions/', authConf.AllowedUsers('admin',['development']), rootProjectsController.getPermissions);
app.post('/api/v1/new_project/', authConf.AllowedUsers('admin',['development']), rootProjectsController.postNewProject);
