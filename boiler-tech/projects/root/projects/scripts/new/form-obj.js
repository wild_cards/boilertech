var formObj = function(projectName, pageName, keyChain){
    var self= this;
    
    
    self.capitalizeFirstLetter = function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    
    self.projectName = projectName;
    self.pageName = pageName;
    self.pagePurpose = '';
    
    self.keyChain = keyChain;
    
    self.isGetPage = false;
    self.isGetData = false;
    self.postPage = [];
    self.postData = [];
    
    self.getPage = {};
    self.getPage.url = "/" + self.projectName + "/" + self.pageName;
    self.getPage.action  = "get" + self.capitalizeFirstLetter(pageName);
    self.getPage.isOAuth = true;
    self.getPage.role = "Webmaster";
    self.getPage.keys = [];
    self.getPage.jsFiles = [];
    self.getPage.cssFiles = [];
    
    self.getData = {};
    self.getData.url = "/api/v1/" + self.projectName + "/" + self.pageName;
    self.getData.action  = "getApi" + self.capitalizeFirstLetter(pageName);
    self.getData.isOAuth = true;
    self.getData.role = "Webmaster";
    self.getData.keys = [];
    
    self.postDataObj = function(projectName, pageName){
        
        this.url = "/api/v1/" + projectName + "/" + pageName;
        this.action = "postApi" + self.capitalizeFirstLetter(pageName);
        this.isOAuth = true;
        this.role = "Webmaster";
        this.keys = [];
        
        this.errorMessage = '';
    }
    
    self.postPageObj = function(projectName, pageName){
        
        this.url = "/" + projectName + "/" + pageName;
        this.action = "post" + self.capitalizeFirstLetter(pageName);
        this.redirectTo = "/" + projectName + "/" + pageName;
        this.isOAuth = true;
        this.role = "Webmaster";
        this.keys = [];
        this.errorMessage = '';
    }
}