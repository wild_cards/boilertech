var rootIndexController = require("~/controller");

app.get('/root/dashboard', passportConf.isAuthenticated, rootIndexController.index);
 