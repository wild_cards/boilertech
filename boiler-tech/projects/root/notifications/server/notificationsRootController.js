var Users = require('./../../../../../this/models/User');
var Notifications = require('./../../../../../this/models/Notification');

exports.getSendNotifications = function(req, res, next) {
    Users.find(function(err, users) {
        res.render('boiler-tech/projects/root/notifications/sendNotifications', {
            title: 'Send Notifications', //,
            users: users,
            // permissions: returningPermissions,
            // webmaster_permissions: returningWebmasterPermissions,
            // selectedRole: selectedRole
        });
    });
}

exports.postSendNotifications = function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');

    req.assert('notificationsMessage', 'A notification message can not be empty.').len(4);

    var errors = req.validationErrors(),
        returningObj = {
            message: 'Error: Something went wrong.',
            notificationsSuccessfullySent: false
        };

    if (errors) {
        returningObj.message = 'A notification message can not be empty';
        res.send(JSON.stringify(returningObj));
        return;
    }



    // if (!req.body.notificationsUsers) {
    //     Users.each(function(user) {
    //         Notifications.sendNotification(user.id, req.body.notificationsMessage, req.body.notificationsUrl);
    //     });
    // }else{
    if (!req.body.notificationsUsers) {
        Users.find(function(err, user) {
            for (var i = 0; i < user.length; i++) {
                Notifications.sendNotification(user[i].id, req.body.notificationsMessage, req.body.notificationsUrl);
            }
        });
    }
    else {
        Users.find({
            email: {
                $in: req.body.notificationsUsers
            }
        }, function(err, user) {
            console.log(err);
            console.log(user);
            console.log(req.body.notificationsUsers);
            for (var i = 0; i < user.length; i++) {
                Notifications.sendNotification(user[i].id, req.body.notificationsMessage, req.body.notificationsUrl);
            }
        });
    }


    returningObj.notificationsSuccessfullySent = true;
    returningObj.message = 'Your notification(s) was sent';

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(returningObj));



    //req.flash('success', "Your notification(s) were sent");

    // console.log(req.body.notificationsMessage);
    console.log(req.body);
    console.log(req.body.notificationsUsers);
}