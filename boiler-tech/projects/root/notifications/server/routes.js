var notificationsRootController = require('~/notificationsRootController');

app.get('/root/sendNotifications', authConf.AllowedUsers('admin'), notificationsRootController.getSendNotifications);
app.post('/root/sendNotifications', authConf.AllowedUsers('admin'), notificationsRootController.postSendNotifications);