$('#selectedUsersView').hide();
var allSelectedUsers = [];

var addUser = function() {
    var userEmail = $('#selectedUsersOptions').val();
    var repeatUser = false;
    for (var i = 0; i < allSelectedUsers.length; i++) {
        if (allSelectedUsers[i] === userEmail) {
            repeatUser = true;
        }

    }
    if (repeatUser) return;
    allSelectedUsers.push(userEmail);
    resetAllSelectedUsersView();
    console.log(userEmail);
}

var removeUser = function(email) {
    for (var i = 0; i < allSelectedUsers.length; i++) {
        if (allSelectedUsers[i] === email) {
            allSelectedUsers.splice(i, 1);
        }

    }
    resetAllSelectedUsersView();
}

var typeOnChange = function() {
    var typeOption = $('#typeOptions').val();
    switch (typeOption) {
        case 'All Users':
            $('#selectedUsersView').hide();
            break;
        case 'Selected Users':
            $('#selectedUsersView').show();
            break;

    }
}

var resetAllSelectedUsersView = function() {
    var counter = 0;
    var tempHtml = '';
    for (var i = 0; i < allSelectedUsers.length; i++) {
        tempHtml += '<p>' + allSelectedUsers[i] + '\
        <span style="color:red;cursor:pointer;text-decoration:underline" onclick="removeUser(\'' + allSelectedUsers[i] + '\')">Remove</span>\
        </p>';
    }
    $('#selectedUsersList').html(tempHtml);
}

var submitNotification = function() {
    $('#sentResult').html('Loading... ');
    $.ajax({
        url: '/root/sendNotifications',
        method: 'POST',
        data: {
            notificationsMessage: $('#notificationsMessage').val(),
            notificationsUrl: $('#notificationsUrl').val(),
            notificationsUsers: allSelectedUsers,
            _csrf: $('#csrf').val()
        },
        success: function(data){
            if(data.notificationsSuccessfullySent){
                $('#sentResult').html('<div class="well bg-success">Notification successfully sent.</div>'); 
                $('#submitNotification').attr('disabled','true');
            } else{
                $('#sentResult').html('<div class="well bg-danger">' + data.message + '.</div>');
            }
        },
        error:function(){
            $('#sentResult').html('<div class="well bg-danger">Something went wrong.</div>');
        }
    });
}