var keys = require('./../../../../../this/models/Key');
var Users = require('./../../../../../this/models/User');
var moment = require('moment');
var secrets = require('./../../../../../this/infrastructure/secrets');


exports.getWebmasterManagekeys = function(req, res) {
  keys.find().sort({ 'name' : 'ascending'}).exec(
    function(err, keys) {
      res.render('boiler-tech/projects/root/manage_keys/webmaster_manage', {
        title: 'Manage keys',
        keys: keys,
        moment: moment
      });
    });
}

exports.getNewkey = function(req, res) {
  res.render('boiler-tech/projects/root/manage_keys/webmaster_create', {
    title: 'Create New key',
    name: '',
    reason: ''
  });
};

exports.postNewkey = function(req, res, next) {
  req.assert('name', '"Name" cannot be blank').notEmpty();
  req.assert('reason', '"Reason for creation" cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/root/create_key/');
  }
  /////////////////////////////////////////////////////////////

  var newkey = new keys({
    name: req.body.name.toLowerCase(),
    isAssignable: req.body.isAssignable
  });
  
  newkey.orignalPurpose.reason = req.body.reason;
  newkey.orignalPurpose.createdByUserId = req.user.id;
  newkey.orignalPurpose.createdBy = req.user.profile.name;
  
  if (req.body.isAssignable) {
    newkey.assignableCreatedBy = req.user.profile.name;
    newkey.assignableCreatedByUserId = req.user.id;
    newkey.assignableCreatedOn = Date.now();
  }


  keys.findOne({ name: req.body.name }, function(err, existingkey) {
    if (existingkey) {
      req.flash('errors', {
        msg: 'key with that name already exists.'
      });
      return res.redirect('/root/create_key');
    }
    newkey.save(function(err) {
      if (err) {
        return next(err);
      }
      else {
        req.flash('success', {
          msg: 'key was successfully created.'
        });
        return res.redirect('/root/manage_keys');
      }
    });
  });
};

exports.getAssignable = function(req, res) {
  res.render('boiler-tech/projects/root/manage_keys/webmaster_assignable', {
    title: 'front',
    key: req.params.key
  });
};

exports.postAssignable = function(req, res, next) {
  keys.findOne({
    name: req.body.key
  }, function(err, doc) {

    if (err) {
      req.flash('error', {
        msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
      });
      return res.redirect('/root/key_is_assignable');
    }

    doc.isAssignable = true;
    doc.assignableCreatedBy = req.user.profile.name;
    doc.assignableCreatedByUserId = req.user.id;
    doc.assignableCreatedOn = Date.now();

    doc.save(function(err) {
      if (err) {
        req.flash('error', {
          msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
        });
      }
      else {
        req.flash('success', {
          msg: 'Admin can now assign the following key: ' + req.body.key
        });
      }
      return res.redirect('/root/manage_keys')
    });
  });
};


exports.getPurpose = function(req, res) {
  res.render('boiler-tech/projects/root/manage_keys/webmaster_purpose', {
    title: 'front',
    key: req.params.key
  });
};

exports.postPurpose = function(req, res, next) {
  req.assert('purpose', '"New purpose for current key" cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);

    return res.redirect('/root/key_purpose/' + req.body.key);
  }
  /////////////////////////////////////////////////////////////

  keys.findOne({
    name: req.body.key
  }, function(err, doc) {

    if (err || doc === null) {
      req.flash('error', {
        msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
      });
      return res.redirect('/root/key_is_assignable');
    }

    doc.newPurpose.push({
      reason: req.body.purpose,
      createdBy: req.user.profile.name,
      createdByUserId: req.user.id,
      createdOn: Date.now()
    });

    doc.save(function(err) {
      if (err) {
        req.flash('error', {
          msg: 'Something went wrong. Admin cannot assign the following key: ' + req.body.key
        });
      }
      else {
        req.flash('success', {
          msg: 'The key called "' + req.body.key + '" has a new purpose. "' + req.body.purpose + '"'
        });
      }
      return res.redirect('/root/manage_keys')
    });
  });
};

