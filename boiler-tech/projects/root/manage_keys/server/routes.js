var rootManageKeysController = require("~/manage-keys-controller");

app.get('/root/manage_keys', authConf.AllowedUsers('webmaster'),rootManageKeysController.getWebmasterManagekeys);
app.get('/root/create_key', authConf.AllowedUsers('webmaster'),rootManageKeysController.getNewkey);
app.post('/root/create_key', authConf.AllowedUsers('webmaster'),rootManageKeysController.postNewkey);
app.get('/root/key_is_assignable/:key', authConf.AllowedUsers('webmaster'),rootManageKeysController.getAssignable);
app.post('/root/key_is_assignable', authConf.AllowedUsers('webmaster'),rootManageKeysController.postAssignable);
app.get('/root/key_purpose/:key', authConf.AllowedUsers('webmaster'),rootManageKeysController.getPurpose);
app.post('/root/key_purpose', authConf.AllowedUsers('webmaster'),rootManageKeysController.postPurpose);
