exports.getAlerts = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/alerts');
};

exports.getAnalytics = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/analytics');
};

exports.getAppPlans = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/app_plans');
};

exports.getButtons = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/buttons');
};

exports.getBlog = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/blog');
};

exports.getBlogDetails = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/blog_details');
};

exports.getCalendar = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/calendar');
};

exports.getChartist = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/chartist');
};

exports.getChartjs = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/chartjs');
};

exports.getChatView = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/chat_view');
};

exports.getCodeEditor = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/code_editor');
};

exports.getContacts = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/contacts');
};

exports.getComponents = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/components');
};

exports.getDatatables = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/datatables');
};

exports.getDraggable = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/draggable');
};

exports.getEmailTemplate = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/email_template');
};

exports.getEmptyStarter = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/empty_starter');
};

exports.getErrorOne = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/error_one');
};

exports.getErrorTwo = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/error_two');
};

exports.getFaq = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/faq');
};

exports.getFileManager = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/file_manager');
};

exports.getFlot = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/flot');
};

exports.getFootable = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/footable');
};

exports.getFormsElements = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/forms_elements');
};

exports.getFormsExtended = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/forms_extended');
};

exports.getForum = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/forum');
};

exports.getForumDetails = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/forum_details');
};

exports.getGallery = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/gallery');
};

exports.getGridSystem = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/grid_system');
};

exports.getIcons = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/icons');
};

exports.getIndex = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/index');
};

exports.getInline = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/inline');
};

exports.getInvoice = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/invoice');
};

exports.getLandingPage = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/landing_page');
};

exports.getLock = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/lock');
};

exports.getLogin = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/login');
};

exports.getMailbox = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/mailbox');
};

exports.getMailboxCompose = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/mailbox_compose');
};

exports.getMailboxView = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/mailbox_view');
};

exports.getModals = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/modals');
};

exports.getNestableList = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/nestable_list');
};

exports.getNotes = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/notes');
};

exports.getOptions = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/options');
};

exports.getOverview = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/overview');
};

exports.getPanels = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/panels');
};

exports.getProfile = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/profile');
};

exports.getProject = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/project');
};

exports.getProjects = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/projects');
};

exports.getRegister = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/register');
};

exports.getSearch = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/search');
};

exports.getSocialBoard = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/social_board');
};

exports.getTablesDesign = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/tables_design');
};

exports.getTextEditor = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/text_editor');
};

exports.getTimeline = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/timeline');
};

exports.getTransitionOne = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/transition_one');
};

exports.getTransitionTwo = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/transition_two');
};

exports.getTransitionThree = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/transition_three');
};

exports.getTransitionFour = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/transition_four');
};

exports.getTransitionFive = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/transition_five');
};

exports.getTour = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/tour');
};

exports.getTypography = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/typography');
};

exports.getValidation = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/validation');
};

exports.getWidgets = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/widgets');
};

exports.getWizard = function(req, res) {
  res.render('boiler-tech/projects/assets/example-app/wizard');
};