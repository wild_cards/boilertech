var fs = require('fs'),
    path = require("path"),
    getAllProjectsDirectories = require("./../tools/getAllProjectsDirectories");

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////   install all routes.js into app.js    /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

exports.init = function(callback) {
    console.log('boiler-tech: Routes will now be built.')
    try{
        var appjs = path.join(__dirname + '../../../app.js'),
        data = fs.readFileSync(appjs, "utf-8"),
        contentArray = data.split('//!l0ad!R0ut3s//Do not Edit');
    
        var blocking = getRoutesFile(function(routes) {
            contentArray[1] = routes;
            var blocking = fs.writeFileSync(appjs, contentArray.join('//!l0ad!R0ut3s//Do not Edit').toString());
            console.log('boiler-tech: App.js has been updated with all routes.');
        });
    } catch(e){
        console.log(e);
    }finally{
    }
    return;
}


function getRoutesFile(callback) {
    var routes = "\n",
        allProjects = getAllProjectsDirectories.init();

    //Take and save the follow: var exampleControler = require("asdf"); and app.get
    for (var i = 0; i < allProjects.length; i++) {
        
        console.log("boiler-tech: Loading route: " + allProjects[i].location + 'server/routes.js');
        var routejs = "";
        try {
            routejs = fs.readFileSync(allProjects[i].location + 'server/routes.js', "utf-8");
        }
        catch (ex) {
            console.log('boiler-tech: **WARNING**: No "/server/routes.js" found in "' + allProjects[i].location + '"');
        }

        if (!routejs || routejs.length < 4) {
            continue;
        }
        var routejsArray = routejs.split('\n');
        for (var j = 0; j < routejsArray.length; j++) {
            if (routejsArray[j].substring(0, 3) == "app") {
                routes += routejsArray[j] + "\n";
            }
            else if (routejsArray[j].substring(0, 3) == "var") {
                //fixing the ~ in controlers
                routejsArray[j] = fixController(routejsArray[j], allProjects[i]);
                routes += routejsArray[j] + "\n";
                
            }
        }

    }
    callback(routes);
}

function fixController(controllerString, projectInfo){
    switch (projectInfo.category) {
        case 'this':
            controllerString = controllerString.replace('~','./../this/projects/' + projectInfo.project + '/' + projectInfo.feature + '/server');
            break;
        case 'root':
            controllerString = controllerString.replace('~','./projects/' + projectInfo.project + '/' + projectInfo.feature + '/server');
            break;
    }
    return controllerString;
}