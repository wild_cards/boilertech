var fs = require('fs'),
    path = require("path"),
    getAllProjectsDirectories = require("./../tools/getAllProjectsDirectories"),
    mkdirp = require("./../tools/mkdir"),
    deleteDir = require("./../tools/deleteDirectory"),
    copyDir = require("./../tools/copyDirectory");

exports.init = function(callback){
    try{
        var allProjects = getAllProjectsDirectories.init();
        
        //The old switch-a-roo..
        
        //1: delete everything in public/this and public/boiler-tech
        
        //2: mk directories for every project in public/this and public/boiler-tech
        var thePath, blocking;
        for (var i = 0; i < allProjects.length; i++) {
            switch (allProjects[i].category) {
                case 'this':
                    thePath = path.join(__dirname + '/../../../public/this/projects/' + allProjects[i].project + '/' + allProjects[i].feature + "/");
                    blocking = deleteDir.init(thePath);
                    blocking = mkdirp.init(thePath);
                    blocking = copyDir.init(allProjects[i].location + "scripts/", thePath + "scripts/");
                    blocking = copyDir.init(allProjects[i].location + "css/", thePath + "css/");
                    break;
                case 'root':
                    thePath = path.join(__dirname + '/../../../public/boiler-tech/projects/' + allProjects[i].project + '/' + allProjects[i].feature + "/");
                    blocking = deleteDir.init(thePath);
                    blocking = mkdirp.init(thePath);
                    blocking = copyDir.init(allProjects[i].location + "scripts", thePath + "scripts");
                    blocking = copyDir.init(allProjects[i].location + "css", thePath + "css");
                    break;
            }
            
        //var blocking = mkdirp(path.join(), function(err) { 

    // path was created unless there was error

        // });

        
        //Things[i]
        }
            
    } catch (e) {
        console.log(e);
    }finally{
    }
    
    //3: copy all the files from the projects
    
    //console.log(getAllProjectsDirectories.init());
    return;
}