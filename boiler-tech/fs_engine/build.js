var fs = require('fs'),
  path = require("path"),
  get_jade_for_views = require("./processes/get_jade_for_views.js"),
  get_js_css_for_public = require("./processes/get_js_css_for_public.js"),
  get_routes_for_app = require("./processes/get_routes_for_app"),
  forever = require('forever-monitor');


exports.init = function(req, res, next) {
  //This is to stop nodemon and run the fs_engine build.js before the program exits.
  process.once('SIGUSR2', function() {
    var complete = false;
    console.log('boiler-tech: restarting.')

    try {
      var step1 = get_js_css_for_public.init();
      var step2 = get_jade_for_views.init();
      var step3 = get_routes_for_app.init();
    }
    catch (es) {
      console.log(es);
    }
    finally {
            complete = true;
      console.log("\n\
| |_ ___|_| |___ ___ ___| |_ ___ ___| |_ \n\
| . | . | | | -_|  _|___|  _| -_|  _|   |\n\
|___|___|_|_|___|_|     |_| |___|___|_|_|\n\
");


      var killServer = function() {
        return process.kill(process.pid, 'SIGUSR2');
      };
      var blah = complete ? killServer() : null;
    }
  });
};
