var fs = require("fs"),
rmdir = require('rimraf');
//rmdir('some/directory/with/files', function(error){});
exports.init = function(path) {
    
    console.log("boiler-tech: deleting everything in: " + path);
    return rmdir.sync(path,[], function(){});
}

var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};