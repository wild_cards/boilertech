var fs = require('fs'),
    path = require("path"),
    getDirectories = require("./getDirectories"),
    allProjects = null;
    
exports.init = function(){
  if(allProjects == null){
    allProjects = getProjectsDirection(allProjects, path.join(__dirname + '/../../../this/projects/'), 'this');
    allProjects = getProjectsDirection(allProjects, path.join(__dirname + '/../../../boiler-tech/projects/'), 'root');
  }
  
  return allProjects;
};

//get all projects/feature in this/projects
function getProjectsDirection(returningArray, projectsPath, category){
  var returningArray = returningArray || [],
      allProjects = getDirectories.init(projectsPath),
      projectArray = [],
      featureArray = [],
      projectPath = '';
  
  if (allProjects instanceof Array) {
    for (var i = 0; i < allProjects.length; i++) {
      if(allProjects[i] != "_infrastructure"){
        projectArray.push(allProjects[i]);
      }
    }
  } else {
    return;
  }
  
  for (var i = 0; i < projectArray.length; i++) {
    projectPath = projectsPath + projectArray[i] + "/";
    featureArray = getDirectories.init(projectPath);
    
    for (var j = 0; j < featureArray.length; j++) {
      if(featureArray[j] != "_layout" && featureArray[j] != "_infrastructure"){
        returningArray.push({
            location: projectPath + featureArray[j] + '/',
            project: projectArray[i],
            feature: featureArray[j],
            category: category
        });
      }
    }
  }
  
  return returningArray;
};

// //get all projects/feature in boilertech/projects
// function getBoilerTechProjects(returningArray){
//   var returningArray = returningArray || [];
//   var allProjects = getDirectories.init(path.join(__dirname + '/../../boiler_tech/_projects'));
//   var projectArray = [];
//   var featureArray = [];
  
//   if (allProjects instanceof Array) {
//     for (var i = 0; i < allProjects.length; i++) {
//       if(allProjects[i] != "_infrastructure"){
//         projectArray.push(allProjects[i]);
//       }
//     }
//   }
  
//   for (var i = 0; i < projectArray.length; i++) {
//     featureArray = getDirectories.init(path.join(__dirname + '/../../boiler_tech/_projects/' + projectArray[i] + "/"));
    
//     for (var j = 0; j < featureArray.length; j++) {
//       if(featureArray[j] != "_layout" && featureArray[j] != "_infrastructure"){
//         returningArray.push('/boiler_tech/_projects/' + projectArray[i] + "/" + featureArray[j]);
//       }
//     }
//   }
  
//   return returningArray;
// }

// //get all projects/feature in catalog/projects
// function getCatalogProjects(returningArray){
//   var returningArray = returningArray || [];
//   var allProjects = getDirectories.init(path.join(__dirname + '/../../catalog/'));
//   var projectArray = [];
//   var featureArray = [];
  
//   if (allProjects instanceof Array) {
//     for (var i = 0; i < allProjects.length; i++) {
//       if(allProjects[i] != allProjects + "_infrastructure"&& featureArray[j] != "_infrastructure"){
//         projectArray.push(allProjects[i]);
//       }
//     }
//   }
  
//   for (var i = 0; i < projectArray.length; i++) {
//     featureArray = getDirectories.init(path.join(__dirname + '/../../catalog/' + projectArray[i] + "/"));
    
//     for (var j = 0; j < featureArray.length; j++) {
//       if(featureArray[j] != "_layout"){
//         returningArray.push('/catalog/' + projectArray[i] + "/" + featureArray[j]);
//       }
//     }
//   }
  
//   return returningArray;
// }