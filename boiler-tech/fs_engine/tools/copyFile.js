var fs = require('fs.extra');

exports.init = function(srcFile, destFile){
    var BUF_LENGTH = 64*1024,
          buff = new Buffer(BUF_LENGTH),
          fdr = fs.openSync(srcFile, 'r'),
          fdw = fs.openSync(destFile, 'w'),
          bytesRead = 1,
          pos = 0,
          temp;
        
    while (bytesRead > 0){
        bytesRead = fs.readSync(fdr, buff, 0, BUF_LENGTH, pos);
        temp = fs.writeSync(fdw,buff,0,bytesRead);
        pos += bytesRead;
    }
    temp = fs.closeSync(fdr);
    temp = fs.closeSync(fdw);
    return;
};