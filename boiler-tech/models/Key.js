var mongoose = require('mongoose');

var keySchema = new mongoose.Schema({
    name: { type : String, lowercase : true },
    isAssignable:{ type: Boolean, default : false },   //should the admin see it and be able to assign it.
    assignableCreatedBy:{ type : String },
    assignableCreatedByUserId:{ type : mongoose.Schema.ObjectId },
    assignableCreatedOn:{ type : Date },
    
    orignalPurpose: {
      reason:{ type : String, lowercase : true },
      createdBy:{ type : String },
      createdByUserId:{ type : mongoose.Schema.ObjectId },
      createdOn:{ type : Date, default : Date.now }
    },
    
    newPurpose:[{
      reason:{ type : String, lowercase : true },
      createdBy:{ type : String },
      createdByUserId:{ type : mongoose.Schema.ObjectId },
      createdOn:{ type : Date, default : Date.now }
    }]
});

module.exports = mongoose.model('keys', keySchema);