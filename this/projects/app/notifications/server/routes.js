var realtimeController = require("~/realtime")(io);


// Real-time notifications and messaging
app.use('/', passportConf.isAuthenticated, realtimeController);