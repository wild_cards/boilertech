var express = require('express');
var router = express.Router();
var Notification = require('../../../../models/Notification');

module.exports = function(io) {
  io.sockets.on('connection', function(socket) {
     console.log(socket);
      
    socket.on('join', function(data) {
      socket.join(data.uid);
    });
  });

  /*
   * GET /app/notifications
   * Fetch notifications by AJAX.
   */
  router.get('/app/notifications', function(req, res) {
    res.writeHead(200, {
      "Content-Type": "application/json"
    });

    Notification.getNotifications(req.user.id, function(result) {
      return res.end(JSON.stringify({
        success: result.success,
        notifications: result.notifications,
        unread: result.unread
      }));
    });
  });

  /*
   * GET /app/notifications
   * Fetch all notifications.
   */
  router.get('/app/allnotifications', function(req, res) {
    Notification.getNotifications(req.user.id, function(result) {
      res.render('this/projects/app/notifications/view', {
        title: 'Notifications',
        notifications: result.notifications.reverse(),
        unread: result.unread
      });
    }, true);
  });

  /*
   * POST /app/readnotification
   * Mark notification as read by AJAX.
   */
  router.post('/app/readnotification', function(req, res) {
    res.writeHead(200, {
      "Content-Type": "application/json"
    });

    Notification.readNotification(req.user.id, req.body.nid, function(result) {
      return res.end(JSON.stringify({
        success: result.success,
        url: result.url,
        unread: result.unread
      }));
    });
  });
  
  router.post('/app/notificationIsRead', function(req, res) {
    res.writeHead(200, {
      "Content-Type": "application/json"
    });

    Notification.notificationIsRead(req.user.id, req.body.nid, function(result) {
      return res.end(JSON.stringify({
        success: result.success
      }));
    });
  });

  /*
   * POST /app/sendnotification
   * Test new notification creation.
   */
  router.post('/app/sendnotification', function(req, res) {
    res.writeHead(200, {
      "Content-Type": "application/json"
    });

    Notification.sendNotification(io, req.user.id, 'You have a new notification');

    return res.end(JSON.stringify({
      success: true
    }));
  });

  return router;
};