
var appAdmin_Controller = require("~/admin-controller");

app.get('/app/company_info', appAdmin_Controller.getCompanyInfo);
app.post('/app/company_info', appAdmin_Controller.postCompanyInfo);

app.get('/app/manage_users', appAdmin_Controller.getManageUsers);
app.get('/app/manage_user', appAdmin_Controller.getManageUser);
app.post('/app/manage_user', appAdmin_Controller.postManageUser);

/*
Copy and paste goodies below. Your code above. 


//var featureController = require("~/feature-controller");

//app.get('/login', featureController.getIndex);
//app.post('/login', featureController.postIndex);


*/