var Company = require('../../../../models/Company');
var User = require('../../../../models/User');

exports.getCompanyInfo = function(req, res) {
  Company.getOne(req.user.selectedCompanyId, req.user.id, req, function(err, company) {
    console.log(req.user.selectedCompanyId);    
    console.log(req.user.id);
    console.log(company);
    res.render('this/projects/app/admin/company_info', {
      title: 'Company Info',
      company: company
    });
  })

};

exports.postCompanyInfo = function(req, res) {
  Company.saveOne(req, function(err, company) {
    return res.redirect('/app/company_info');
  })

};

exports.getManageUsers = function(req, res) {
  User.getBySelectedCompany(req.user.selectedCompanyId, function(err, users) {
    console.log('USERS');
    console.log(users);
    res.render('this/projects/app/admin/manage_users', {
      title: 'Manage Users',
      users: users
    });
  })
};

exports.getManageUser = function(req, res) {
  console.log(req.query);
  User.getUserWithCompany(req.user.selectedCompanyId, req.user.id, function(err, user) {
    res.render('this/projects/app/admin/manage_user', {
      title: 'Manage Users',
      theUser: user
    });
  })
};

exports.postManageUser = function(req, res) {
  res.redirect('this/projects/app/admin/manage_users', {
    title: 'Manage Users'
  });
};

/*
exports.getIndex = function(req, res) {
  res.render('this/projects/_projectName/_feature/index', {
    title: 'Dashboard'
  });
};

exports.postSignup = function(req, res, next) {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/signup');
  }

  var user = new User({
    email: req.body.email,
    password: req.body.password,
    username: req.body.username
  });

  User.findOne({ email: req.body.email }, function(err, existingUser) {
    if (existingUser) {
      req.flash('errors', { msg: 'Account with that email address already exists.' });
      return res.redirect('/signup');
    }
    user.save(function(err) {
      if (err) {
        return next(err);
      }
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
        res.redirect('/app/index');
      });
    });
  });
};


*/