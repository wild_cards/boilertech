'use strict';
var Company = require('./../../../../../this/models/Company');
var Invoice = require('./../../../../../this/models/Invoice');


var secrets = require('./../../../../../this/infrastructure/secrets');

var plans = Company.getPlans();

exports.getDefault = function(req, res, next) {
    var form = {},
        error = null,
        formFlash = req.flash('form'),
        errorFlash = req.flash('error');

    if (formFlash.length) {
        form.email = formFlash[0].email;
    }
    if (errorFlash.length) {
        error = errorFlash[0];
    }
    console.log(plans);
    Company.getOne(req.user.selectedCompanyId, req.user.id, req, function(err, company) {
        res.render("this/projects/app/payment/dashboard-sample", {
            company: company,
            form: form,
            error: error,
            plans: plans,
            stripePubKey: secrets.stripeOptions.stripePubKey
        });
    });
};

exports.getBilling = function(req, res, next) {
    var form = {},
        error = null,
        formFlash = req.flash('form'),
        errorFlash = req.flash('error');

    if (formFlash.length) {
        form.email = formFlash[0].email;
    }
    if (errorFlash.length) {
        error = errorFlash[0];
    }
    Company.getOne(req.user.selectedCompanyId, req.user.id, req, function(err, company) {
        console.log(company);
        res.render("this/projects/app/payment/app/payment_center-sample", {
            company: company,
            form: form,
            error: error,
            plans: plans
        });
    });
};

exports.getProfile = function(req, res, next) {
    var form = {},
        error = null,
        formFlash = req.flash('form'),
        errorFlash = req.flash('error');

    if (formFlash.length) {
        form.email = formFlash[0].email;
    }
    if (errorFlash.length) {
        error = errorFlash[0];
    }

    res.render(req.render, {
        user: req.user,
        form: form,
        error: error,
        plans: plans
    });
};



exports.postBilling = function(req, res, next) {
    var stripeToken = req.body.stripeToken;

    if (!stripeToken) {
        req.flash('errors', {
            msg: 'Please provide a valid card.'
        });
        return res.redirect('/app/payment_center');
    }

    Company.findById(req.user.selectedCompanyId, function(err, company) {
        if (err) return next(err);

        company.setCard(stripeToken, function(err) {
            if (err) {
                if (err.code && err.code == 'card_declined') {
                    req.flash('errors', {
                        msg: 'Your card was declined. Please provide a valid card.'
                    });
                    return res.redirect('/app/payment_center');
                }
                req.flash('errors', {
                    msg: 'An unexpected error occurred.'
                });
                return res.redirect('/app/payment_center');
            }
            req.flash('success', {
                msg: 'Billing has been updated.'
            });
            res.redirect('/app/payment_center');
        });
    });
};

exports.postPlan = function(req, res, next) {
    var plan = req.body.plan;
    var stripeToken = null;

    if (plan) {
        plan = plan.toLowerCase();
    }
    Company.getOne_advance(req.user.selectedCompanyId, function(err, company) {
        if (company.stripe.plan == plan) {
            req.flash('info', {
                msg: 'The selected plan is the same as the current plan.'
            });
            return res.redirect('/app/payment_center');
        }
        console.log('\n\ntoken:\n\n' + req.body.stripeToken);
        if (req.body.stripeToken) {
            stripeToken = req.body.stripeToken;
        }

        if (!company.stripe.last4 && !req.body.stripeToken) {
            req.flash('errors', {
                msg: 'Please add a card to your account before choosing a plan.'
            });
            return res.redirect('/app/payment_center');
        }

        if (err) return next(err);

        company.setPlan(plan, stripeToken, function(err) {
            console.log(plan);
            var msg;

            if (err) {
                if (err.code && err.code == 'card_declined') {
                    msg = 'Your card was declined. Please provide a valid card.';
                }
                else if (err && err.message) {
                    msg = err.message;
                }
                else {
                    msg = 'An unexpected error occurred.';
                }

                req.flash('errors', {
                    msg: msg
                });
                return res.redirect('/app/payment_center');
            }
            if (plan == 'free') {

                req.flash('success', {
                    msg: 'Your account has been deactivated.'
                });

                res.redirect('/app/payment_center');


            }
            else {

                req.flash('success', {
                    msg: 'Your account is now active.'
                });

                res.redirect('/app/payment_center');


            }

        });
    });

};