var Company = require('../../../../models/Company');
var Invoice = require('../../../../models/Invoice');
var InvoiceIssue = require('../../../../models/Invoice_Issue');
var InvoiceReciept = require('../../../../models/Invoice_Reciept');

var plans = Company.getPlans();

//sk_test_ngnzGjc7TIUcvNFzbCq83omJ
var stripe = require("stripe")("sk_test_ngnzGjc7TIUcvNFzbCq83omJ");

exports.getCharge = function(req, res, next) {
	Company.getOne(req.user.selectedCompanyId, req.user.id, req, function(err1, company) {
		Invoice.getOne(company.currentInvoiceId, function(err2, invoice) {
			var stripeToken = req.body.stripeToken;

			var charge = stripe.charges.create({
				amount: parseInt(invoice.total.toFixed(2).toString().split(".").join("")), // amount in cents, again
				currency: "usd",
				card: stripeToken,
				description: "invoice #: " + invoice.id
			}, function(err, charge) {
				console.log('CHARGE\n\n');
				console.log(charge);
				if (err && err.type === 'StripeCardError') {
					// The card has been declined
					req.flash('danger', {
						msg: 'This card did not process. Please try another card.'
					});

					return res.redirect('/app/payment_center');


				}
				else {
					Invoice.successfullyPayed(req, charge, function() {
						//Render a thank you page called "Charge"
						res.render('this/projects/app/payment/payment_thankyou', {
							title: 'Charge'
						});
					});

				};
			});


		});
	});

};

exports.getPaymentHome = function(req, res, next) {
	Company.getOne(req.user.selectedCompanyId, req.user.id, req, function(err1, company) {
		Invoice.getOne(company.currentInvoiceId, function(err2, invoice) {
			res.render('this/projects/app/payment/payment_home', {
				title: 'Payment Center',
				company: company,
				invoice: invoice,
				plans: plans
			});
		});
	});

}

exports.getPaymentIssue = function(req, res, next) {
	InvoiceIssue.getOne({
		_id: req.query.id
	}, function(err1, invoiceIssue) {
		res.render('this/projects/app/payment/payment_issue', {
			title: 'Invoice Issue',
			invoiceIssueId: req.query.id || "",
			invoiceId: req.query.invoiceId || "",
			invoiceIssue: invoiceIssue
		});

	});
}

exports.postPaymentIssue = function(req, res, next) {
	Company.getOne(req.user.selectedCompanyId, req.user.id, req, function(err1, company) {
		InvoiceIssue.saveOne(
			req.body.invoiceIssueId,
			req.user.selectedCompanyId,
			req.body.invoiceId || company.currentInvoiceId,
			req.user.id, req.body.message, req.user.profile.name,
			function(err2, invoiceIssue) {
				if (err1) {
					return next(err1);
				}
				else if (err2) {
					return next(err2);
				}
				else {
					req.flash('success', {
						msg: 'Invoice issue was successfully reported.'
					});

					return res.redirect('/app/payment_center');
				}
			});
	});
}

exports.getRecieptViewable = function(req,res, next){
	InvoiceReciept.getOne(req.query.id, function(err, invoice) {
		res.render('this/projects/app/payment/payment_reciept_invoice', {
			title: 'Reciept',
			invoice: invoice
		});
	});	
}

exports.getPaymentInvoiceViewable = function(req, res, next) {
	Invoice.getOne(req.query.id, function(err, invoice) {
		res.render('this/projects/app/payment/payment_view_invoice', {
			title: 'Invoice',
			invoice: invoice
		});
	});
}