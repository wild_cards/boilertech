var AppPaymentControrller = require('~/payment-controller');
var appBillingController = require('~/billingController');

app.get('/app/payment_center', passportConf.isAuthenticated, AppPaymentControrller.getPaymentHome);
app.get('/app/payment_issue', passportConf.isAuthenticated, AppPaymentControrller.getPaymentIssue);
app.post('/app/payment_issue', passportConf.isAuthenticated, AppPaymentControrller.postPaymentIssue);

app.get('/app/payment_view_invoice', passportConf.isAuthenticated, AppPaymentControrller.getPaymentInvoiceViewable);
app.get('/app/payment_view_reciept', passportConf.isAuthenticated, AppPaymentControrller.getRecieptViewable);
app.post('/app/charge', passportConf.isAuthenticated, AppPaymentControrller.getCharge);

app.get('/dashboard', passportConf.isAuthenticated, appBillingController.getDefault);
app.get('/billing', passportConf.isAuthenticated, appBillingController.getBilling);

app.post('/app/billing', passportConf.isAuthenticated, appBillingController.postBilling);
app.post('/app/plan', passportConf.isAuthenticated, appBillingController.postPlan);
