/**
 * GET /
 * Home page.
 */
exports.index = function(req, res) {
  res.render('this/projects/main/frontpage/index', {
    title: 'front'
  });
};
