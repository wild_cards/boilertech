var isRequestAppointment = function(){
    var isRequestAppointmentBool = $('#requestAppointment').is(":checked");
    if(isRequestAppointmentBool){
        $('#appointment').show();
    } else {
        $('#appointment').hide();
    }
    
};

isRequestAppointment();


$(function() {
    $('#datepicker').datepicker();
    $("#datepicker").on("changeDate", function(event) {
        $("#my_hidden_input").val(
            $("#datepicker").datepicker('getFormattedDate')
        )
    });

    $('#datapicker2').datepicker();
    $('.input-group.date').datepicker({});
    $('.input-daterange').datepicker({});
    
});