var Company_Site = require('../../../../models/Company_Site');
var Company = require('../../../../models/Company');
var Company_Site_Lead = require('../../../../models/Company_Site_Lead');

exports.getSiteViewerContact = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                console.log(company);
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "contact",
                    company: company,
                    title: 'Contact Us'
                });
            });
        });
}

exports.getSiteViewerHome = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "home",
                    company: company,
                    title: 'Home'
                });
            });
        });
}

exports.getSiteViewerPictures = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "pictures",
                    company: company,
                    title: 'Pictures'
                });
            });
        });
}

exports.getSiteViewerProjects = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "projects",
                    company: company,
                    title: 'Projects'
                });
            });
        });
}

exports.getSiteViewerReviews = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "reviews",
                    company: company,
                    title: 'Reviews'
                });
            });
        });
}

exports.getSiteViewerServices = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "services",
                    company: company,
                    title: 'Services'
                });
            });
        });
}

exports.getSiteViewerThankYou = function(req, res, next) {
    Company_Site.getOne(req, res, next,
        function(site) {
            Company.getOne_advance(site.companyId, function(err, company) {
                res.render("this/projects/site/viewer/foundation_viewer", {
                    site: site,
                    site_page: "thankyou",
                    company: company,
                    title: 'Thank You'
                });
            });
        });
}




exports.postSiteLead = function(req, res, next) {
    console.log(req.body);
    Company_Site_Lead.getOne(req, res, next, function(companySiteLead) {
        companySiteLead.leads.push({
            customer: {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                phoneNumber: req.body.phoneNumber
            },
            message: req.body.message,
            requestAppointment: req.body.requestAppointment || false,
            appointment: {
                firstChoice: req.body.firstChoice,
                secondChoice: req.body.secondChoice,
                thirdChoice: req.body.thirdChoice
            }

        });
        companySiteLead.save(function(err) {

            res.redirect('thankyou');

        });
    });
}
