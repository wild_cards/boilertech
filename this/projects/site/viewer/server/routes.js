
var siteViewer_Controller = require("~/thisSiteViewerController");

app.get('/sites/:id/', siteViewer_Controller.getSiteViewerHome);
app.get('/sites/:id/home', siteViewer_Controller.getSiteViewerHome);
app.get('/sites/:id/contact', siteViewer_Controller.getSiteViewerContact);
app.get('/sites/:id/projects', siteViewer_Controller.getSiteViewerProjects);
app.get('/sites/:id/pictures', siteViewer_Controller.getSiteViewerPictures);
app.get('/sites/:id/reviews', siteViewer_Controller.getSiteViewerReviews);
app.get('/sites/:id/services', siteViewer_Controller.getSiteViewerServices);
app.get('/sites/:id/thankyou', siteViewer_Controller.getSiteViewerThankYou);
app.post('/sites/:id/lead', siteViewer_Controller.postSiteLead)