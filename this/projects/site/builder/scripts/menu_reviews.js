var ViewModel = function(reviewsObj){
    var self = this;
    console.log(reviewsObj);
    self.reviews = ko.observableArray(reviewsObj);
    
    self.addReview = function() {
        self.reviews.push({ message: "", name: "", location: "" });
    };
 
    self.removeReview = function() {
        self.reviews.remove(this);
    };
    
    self.init = function(){
        if(self.reviews() < 1){
            self.addReview();
        }
    };
    
    self.init();
};