var modeSelect = function() {
    var optionalParams = "";
    if(!(typeof(app_colors) == 'undefined') && app_colors){
        optionalParams += "&colors=" + app_colors;
    }
    window.location.href = '/site/builder?mode=' + $('#siteBuilderSelect').val() + '&location=' + app_location + optionalParams;
}