var Company_Site = require('../../../../models/Company_Site');
var Company = require('../../../../models/Company');
var App_Setting = require('../../../../models/App_Setting');

exports.getSite = function(req, res, next) {
  Company.getOne_advance(req.user.selectedCompanyId, function(err, company) {
    res.redirect('/sites/' + company.siteId + '/');
  });
}
exports.getSiteBuilder = function(req, res, next) {
  Company_Site.getOne_advance(req, res, next,
    function(site) {
      Company.getOne_advance(site.companyId, function(err, company) {
        var renderView = req.query.location;
        var site_page = "home";
        var mode = req.query.mode;

        switch (renderView) {
          case 'home':
            renderView = 'this/projects/site/builder/builder_home';
            site_page = 'home';
            break;
          case 'about':
            renderView = 'this/projects/site/builder/builder_about';
            site_page = 'about';
            break;
          case 'services':
            renderView = 'this/projects/site/builder/builder_services';
            site_page = 'services';
            break;
          case 'pictures':
            renderView = 'this/projects/site/builder/builder_pictures';
            site_page = 'pictures';
            break;
          case 'projects':
            renderView = 'this/projects/site/builder/builder_projects';
            site_page = 'projects';
            break;
          case 'reviews':
            renderView = 'this/projects/site/builder/builder_reviews';
            site_page = 'reviews';
            break;
          case 'contact':
            renderView = 'this/projects/site/builder/builder_contact';
            site_page = 'contact';
            break;
          default:
            renderView = 'this/projects/site/builder/builder_home';
            break;
        }

        switch (mode) {
          //not included: top, colors
          case 'pictures':
            renderView = 'this/projects/site/builder/menu_pictures';
            break;
          case 'projects':
            renderView = 'this/projects/site/builder/menu_projects';
            break;
          case 'reviews':
            renderView = 'this/projects/site/builder/menu_reviews';
            break;
          case 'slideshow':
            renderView = 'this/projects/site/builder/menu_slideshow';
            break;
          case 'services':
            renderView = 'this/projects/site/builder/menu_services';
            break;
          case 'bio':
            renderView = 'this/projects/site/builder/menu_bio';
            break;

        }

        res.render(renderView, {
          mode: req.query.mode || "top",
          location: req.query.location || "home",
          colors: req.query.colors || null,
          site_page: site_page,
          site: site,
          company: company,
          title: 'Site Builder'
        });
      });
    });
};

exports.postChangeColors = function(req, res, next) {
  Company_Site.getOne_advance(req, res, next,
    function(site) {
      site.colors = req.body.colors;
      site.save(function() {
        res.redirect('/site/builder?mode=top&location=' + req.body.location);
      });
    });
}

exports.postManageBio = function(req, res, next) {
  console.log(req.body.paragraphs);
  Company_Site.getOne_advance(req, res, next,
    function(site) {
      site.bio.title = req.body.title;
      if (req.body.paragraphs) {
        site.bio.paragraphs = [];
        for (var i = 0; i < req.body.paragraphs.length; i++) {
          site.bio.paragraphs.push({
            line: req.body.paragraphs[i],
            timestamp: new Date,
            createdBy: req.user.id
          });
        }
      }

      site.save(function(err) {
        res.redirect('/site/builder?mode=top&location=' + req.body.location);

      })
    });
}


exports.postManageReviews = function(req, res, next) {
  Company_Site.getOne_advance(req, res, next,
    function(site) {
      if (req.body.reviews) {
        site.reviews = [];
        for (var i = 0; i < req.body.reviews.length; i++) {
          site.reviews.push({
            message: req.body.reviews[i][0],
            name: req.body.reviews[i][1],
            location: req.body.reviews[i][2],
            timestamp: new Date,
            createdBy: req.user.id
          });
        }
      }

      site.save(function(err) {
        res.redirect('/site/builder?mode=top&location=' + req.body.location);

      })
    });
}


exports.postManageServices = function(req, res, next) {
  Company_Site.getOne_advance(req, res, next,
    function(site) {
      site.services = [];
      if (req.body.services) {
        for (var i = 0; i < req.body.services.length; i++) {
          site.services.push({
            name: req.body.services[i][0],
            description: req.body.services[i][1],
            timestamp: new Date,
            createdBy: req.user.id
          });
        }
      }

      site.save(function(err) {
        res.redirect('/site/builder?mode=top&location=' + req.body.location);

      })
    });
}