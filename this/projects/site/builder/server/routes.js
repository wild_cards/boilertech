
var siteBuilder_Controller = require("~/thisSiteBuilderController");

app.get('/site/builder', siteBuilder_Controller.getSiteBuilder);
app.get('/site/get-site', siteBuilder_Controller.getSite);

app.post('/site/change-colors', siteBuilder_Controller.postChangeColors);

app.post('/site/manage-bio', siteBuilder_Controller.postManageBio);
app.post('/site/manage-reviews', siteBuilder_Controller.postManageReviews);
app.post('/site/manage-services', siteBuilder_Controller.postManageServices);

