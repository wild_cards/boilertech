var Company_Site_Lead = require('../../../../models/Company_Site_Lead');

exports.getSiteLeads = function(req, res, next) {
    Company_Site_Lead.getOne_advance(req, res, next, function(companySiteLead) {
        console.log(companySiteLead);
        res.render("this/projects/marketing/leads/view_all", {
            companySiteLead: companySiteLead,
            title: 'Leads'
        });
    });
}

exports.getSiteLead = function(req, res, next) {
    Company_Site_Lead.getOne_advance(req, res, next, function(companySiteLead) {
        var lead = {};
        for (var i = 0; i < companySiteLead.leads.length; i++) {
            if (companySiteLead.leads[i].id == req.query.id) {
                console.log(companySiteLead.leads[i]);
                lead = companySiteLead.leads[i]

            }
        }

        res.render("this/projects/marketing/leads/view", {
            title: 'Leads',
            lead: lead
        });
    });
}