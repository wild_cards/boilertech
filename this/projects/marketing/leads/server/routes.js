var marketingLead_Controller = require("~/marketing_lead_controller");

app.get('/marketing/site-leads', marketingLead_Controller.getSiteLeads);
app.get('/marketing/site-lead', marketingLead_Controller.getSiteLead);
