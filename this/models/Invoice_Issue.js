var mongoose = require('mongoose');
var Invoice = require("./Invoice");

var invoiceIssueSchema = new mongoose.Schema({

    companyId: {
        type: mongoose.Schema.ObjectId
    },
    invoiceId: {
        type: mongoose.Schema.ObjectId
    },
    mainMessage: {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        message: {
            type: String
        },
        timestamp: {
            type: Date
        }
    },
    messages: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        message: {
            type: String
        },
        timestamp: {
            type: Date
        }
    }],
});

invoiceIssueSchema.statics.getOne = function(invoiceIssueId, cb) {
    this.findOne({
            _id: invoiceIssueId
        }).populate('mainMessage.user')
        .populate('messages.user').exec(function(err, invoiceIssue) {
            console.log(invoiceIssue);
            cb(err, invoiceIssue);
        })
};

invoiceIssueSchema.statics.saveOne = function(invoiceIssueId, companyId, invoiceId, userId, message, name, cb) {
    console.log('UserId');

    var tempInvoiceIssue = new this();
    if (invoiceIssueId == "") {
        invoiceIssueId = null;
    }
    if (invoiceId == "") {
        invoiceId = null;
    }
    this.findOne({
        _id: invoiceIssueId
    }, function(err1, invoiceIssue) {
        console.log(err1);
        invoiceIssue = invoiceIssue || tempInvoiceIssue;
        console.log(invoiceIssue);
        invoiceIssue.invoiceId = invoiceId;
        if (!companyId || companyId != '') {
            invoiceIssue.companyId = companyId;
        }
        if (invoiceIssue.mainMessage.message == null) {
            invoiceIssue.mainMessage = {
                user: userId,
                message: message,
                timestamp: new Date()
            };
        }
        else {
            invoiceIssue.messages.push({
                user: userId,
                message: message,
                timestamp: new Date()
            });

        }

        invoiceIssue.save(function(err2) {
            console.log(err2);
            if (invoiceIssue.invoiceId) {
                Invoice.findOne({
                    _id: invoiceIssue.invoiceId
                }, function(err3, invoice) {
                    console.log(err3);
                    invoice.invoiceIssueId = invoiceIssue.id;
                    invoice.invoiceIssue_allMessagesRead = true;
                    invoice.save(function(err4) {
                        console.log(err4);
                        cb(err3, invoiceIssue);
                    });
                });
            }
            else {

                cb(err1, invoiceIssue);
            }
        });
    });
};

invoiceIssueSchema.statics.saveOne_advance = function(invoiceIssueId,
    companyId, invoiceId, userId, message, name, cb) {
    var tempInvoiceIssue = new this();

    this.findOne({
        _id: invoiceIssueId
    }, function(err1, invoiceIssue) {

        invoiceIssue = invoiceIssue || tempInvoiceIssue;

        invoiceIssue.invoiceId = invoiceIssue.invoiceId || invoiceId;
        if (!companyId || companyId != '' || !invoiceIssue.companyId) {
            invoiceIssue.companyId = companyId;
        }
        if (invoiceIssue.mainMessage.message == null) {
            invoiceIssue.mainMessage = {
                user: userId,
                message: message,
                timestamp: new Date()
            };
        }
        else {
            invoiceIssue.messages.push({
                user: userId,
                message: message,
                timestamp: new Date()
            });

        }

        invoiceIssue.save(function(err2) {
            console.log(err2);
            if (invoiceIssue.invoiceId) {
                Invoice.findOne({
                    _id: invoiceIssue.invoiceId
                }, function(err3, invoice) {
                    console.log(err3);
                    invoice.invoiceIssueId = invoiceIssue.id;
                    invoice.invoiceIssue_allMessagesRead = false;
                    invoice.save(function(err4) {
                        console.log(err4);
                        cb(err3, invoiceIssue);
                    });
                });
            }
            else {

                cb(err1, invoiceIssue);
            }
        });
    });
};

module.exports = mongoose.model('Invoice_Issue', invoiceIssueSchema);