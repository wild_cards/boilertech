//This is a stack. I am using it as a "schema" for company related collections. They all connected to this collection
var mongoose = require('mongoose');
var User = require('./User');
var stripeCustomer = require('./../infrastructure/stripe-customer');
var secrets = require('./../infrastructure/secrets');
var stripeOptions = secrets.stripeOptions;
var timestamps = require('mongoose-timestamp');

var CompanySchema = new mongoose.Schema({
    //site
    siteId: {
        type: mongoose.Schema.Types.ObjectId
    },

    //payment
    currentInvoiceId: {
        type: mongoose.Schema.Types.ObjectId
    },
    currentRecieptId: {
        type: mongoose.Schema.Types.ObjectId
    },
    chargeDate: {
        type: Number
    },
    sendInvoiceDate: {
        type: Number
    },
    hasInvoiceIssue: {
        type: Boolean,
        default: false
    },
    pastDueInvoices: [{
        service_period_start: {
            type: Date
        },
        service_period_end: {
            type: Date
        },
        total: {
            type: Number
        },
        invoiceId: {
            type: Number
        }
    }],
    pastInvoices: [{
        service_period_start: {
            type: String
        },
        service_period_end: {
            type: String
        },
        invoiceStatus: {
            type: String
        },
        payedOnDate: {
            type: Date
        },
        invoiceId: {
            type: mongoose.Schema.Types.ObjectId
        },
        recieptId: {
            type: mongoose.Schema.Types.ObjectId
        }
    }],



    //info
    company_name: {
        type: String,
        default: 'examplesite.com'
    },
    company_phone: {
        type: String,
        default: ''
    },
    company_email: {
        type: String,
        default: ''
    },
    company_address1: {
        type: String,
        default: "examplesite.com"
    },
    company_address2: {
        type: String,
        default: "P.O. Box 123"
    },
    company_city: {
        type: String,
        default: "Arcadia"
    },
    company_state: {
        type: String,
        default: "CA"
    },
    company_zip: {
        type: String,
        default: "91007"
    },
    company_url: {
        type: String,
        default: ''
    },
    company_isActive: {
        type: Boolean,
        default: false
    },
    company_isGoodOnPayments: {
        type: Boolean,
        default: true
    },


    //social media links
    company_facebook: {
        type: String,
        default: ""
    },

    company_twitter: {
        type: String,
        default: ""
    },

    company_yelp: {
        type: String,
        default: ""
    },

    company_instagram: {
        type: String,
        default: ""
    },

    company_angieList: {
        type: String,
        default: ""
    },
    company_users: [{
        type: mongoose.Schema.Types.ObjectId
    }],

    users_can_view: [{
        type: mongoose.Schema.Types.ObjectId
    }],
    users_can_edit: [{
        type: mongoose.Schema.Types.ObjectId
    }],


});

CompanySchema.plugin(timestamps);
CompanySchema.plugin(stripeCustomer, stripeOptions);

CompanySchema.statics.getOne = function(companyId, userId, req, cb) {
    if (req.user.userSiteAccess.powerUser) {
        this.findOne({
            _id: req.user.selectedCompanyId
        }, function(err, company) {
            cb(err, company);
        });
    }
    else {

        this.findOne({
            $and: [{
                _id: companyId
            }, {
                users_can_view: userId
            }]
        }).exec(function(err, company) {

            cb(err, company);

        });
    }
}

CompanySchema.statics.getOne_advance = function(companyId, cb) {
    this.findOne({
        _id: companyId
    }, function(err, company) {
        cb(err, company);
    });
}

CompanySchema.statics.getAll = function(cb) {
    this.find(function(err, companies) {
        cb(err, companies);
    });
};

CompanySchema.statics.getAllCompanyUsers = function(companyId, cb) {
    User.find({
        selectedCompanyId: companyId
    }, function(err, company) {
        cb(err, company);
    });
};

CompanySchema.statics.getUsersWithNoSelectedCompany = function(companyId, cb) {
    User.find({
        selectedCompanyId: null
    }, function(err, company) {
        cb(err, company);
    });
}

CompanySchema.statics.saveOne = function(req, cb) {
    var newCompany = new this();
    console.log('asdf');

    this.findOne({
        _id: req.user.selectedCompanyId,
        users_can_view: req.user.id
    }, function(err, company) {
        console.log('COMPANY');
        console.log(req);
        company = company || newCompany;
        company.company_url = req.body.company_url;
        company.company_name = req.body.company_name;
        company.company_address1 = req.body.company_address1;
        company.company_address2 = req.body.company_address2;
        company.company_city = req.body.company_city;
        company.company_state = req.body.company_state;
        company.company_zip = req.body.company_zip;
        company.company_phone = req.body.company_phone;
        company.company_email = req.body.company_email;
        company.company_facebook = req.body.company_facebook;
        company.company_twitter = req.body.company_twitter;
        company.company_yelp = req.body.company_yelp;
        company.company_instagram = req.body.company_instagram;
        company.company_angieList = req.body.company_angieList;


        company.save(function(err) {
            cb(err);
        });
    });

};
CompanySchema.statics.firstSave = function(req, cb) {
    var company = new this();

    company.company_name = req.body.company_name;
    company.company_address1 = req.body.company_address1;
    company.company_address2 = req.body.company_address2;
    company.company_city = req.body.company_city;
    company.company_state = req.body.company_state;
    company.company_zip = req.body.company_zip;

    company.save(function(err) {
        cb(err);
    });

};

module.exports = mongoose.model('Company', CompanySchema);