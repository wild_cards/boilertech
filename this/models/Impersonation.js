var mongoose = require('mongoose');
var moment = require('moment');
var User = require('./../../this//models/User');
var passport = require('passport');

var impersonationTimeLimit = 30; //minutes


var impersonationSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  timestamp: {
    type: Date,
    default: null
  },
  isImpersonationInProgress: {
    type: Boolean,
    default: false
  },
  userRequestingImpersonation: String,
  type: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  userBeingImpersonate: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  pastNotifications: [{
    timestamp: {
      type: Date,
      default: Date.now
    },
    isImpersonationInProgress: {
      type: Boolean,
      default: false
    },
    userRequestingImpersonation: String,
    type: {
      type: String,
      default: ''
    },
    userBeingImpersonate: {
      type: String,
      default: ''
    }
  }],
});

impersonationSchema.statics.startAnImpersonation = function(userBeingImpersonateId, userRequestingImpersonationId) {
  var impersonation = new this();
  impersonation.isImpersonationInProgress = true;
  impersonation.userRequestingImpersonation = userBeingImpersonateId;
  impersonation.userBeingImpersonate = userRequestingImpersonationId;
  impersonation.timestamp = Date.now();

  var blocking = impersonation.save(function() {

  });
}

impersonationSchema.statics.endAnImpersonation = function(userBeingImpersonate, req, res, next) {
  this.findOne({
    userBeingImpersonate, userBeingImpersonate
  }, function(err, impersonation) {
    User.findOne({
      _id: impersonation.userRequestingImpersonation
    }, function(err, user) {
      console.log(userBeingImpersonate);
      var temp = req.user.id;
      console.log('userId: ' + req.user.id);
      var blocking = req.logout();
      req.logIn(user, function(err) {
        console.log('new user logged in:');
        //console.log(existingUser);
        if (err) {
          console.log('error');
        }
        console.log('userId: ' + req.user.id);
        req.session.user = req.user;
        req.session.impersonationInProgress = null;
        blocking = passport.session();
        blocking = passport.initialize();


        impersonation.save(function() {



          impersonation.pastNotifications.push({
            isImpersonationInProgress: impersonation.isImpersonationInProgress,
            userRequestingImpersonation: impersonation.userRequestingImpersonation,
            userBeingImpersonate: impersonation.userBeingImpersonate,
            timestamp: impersonation.timestamp
          });

          impersonation.isImpersonationInProgress = null;
          impersonation.userRequestingImpersonation = null;
          impersonation.userBeingImpersonate = null;
          impersonation.timestamp = null;

          return res.redirect('/app/dashboard');;
        });
      });

    });
  });
}

module.exports = mongoose.model('Impersonation', impersonationSchema);
