var mongoose = require('mongoose');
var company = require("./Company");

var CompanySiteSchema = new mongoose.Schema({
    companyId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    colors: {
        type: String,
        default: 'cerulean'
    },
    bio: {
        title: {
            type: String
        },
        paragraphs: [{
            line: {
                type: String
            },
            timestamp: {
                type: Date
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }],

        updates: [{
            title: {
                type: String
            },
            paragraphs: [{
                line: {
                    type: String
                },
                timestamp: {
                    type: Date
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            }],
            timestamp: {
                type: Date
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }]
    },
    projects: {
        all_examples: [{
            file_upload_id: {
                type: mongoose.Schema.Types.ObjectId
            },
            tags: [{
                type: String
            }],
            timestamp: {
                type: Date
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            updates: [{
                file_upload_id: {
                    type: mongoose.Schema.Types.ObjectId
                },
                tags: [{
                    type: String
                }],
                timestamp: {
                    type: Date
                },
                createdBy: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'User'
                }
            }]
        }],
        before_and_after: [{
            project_name: {
                type: String
            },
            project_desc: {
                type: String
            },
            before: [{
                file_upload_id: {
                    type: mongoose.Schema.Types.ObjectId
                },
                tags: [{
                    type: String
                }]
            }],
            after: [{
                file_upload_id: {
                    type: mongoose.Schema.Types.ObjectId
                },
                tags: [{
                    type: String
                }]
            }]
        }]
    },
    services: [{
        name: {
            type: String
        },
        description: {
            type: String
        },
        timestamp: {
            type: Date
        },
        createdBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        updates: [{
            name: {
                type: String
            },
            description: {
                type: String
            },
            timestamp: {
                type: Date
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }

        }]
    }],
    reviews: [{
        message: {
            type: String
        },
        name: {
            type: String
        },
        location: {
            type: String
        },
        timestamp: {
            type: Date
        },
        createdBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        updates: [{
            message: {
                type: String
            },
            name: {
                type: String
            },
            locationCity: {
                type: String
            },
            locationState: {
                type: String
            },
            timestamp: {
                type: Date
            },
            createdBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }
        }]
    }]
});

CompanySiteSchema.statics.getOne = function(req, res, next, cb) {
    console.log(req.params);
    var newThisCollection = new this();
    this.findOne({
            _id: req.params.id || null
        },
        function(err, site) {
            console.log(err);
            console.log(site);
            if (!site) {
                createNewCollection(req, res, next, newThisCollection,
                    function() {
                        cb(newThisCollection);
                    }
                );
            }
            else {
                cb(site);
            };
        }
    );
};
CompanySiteSchema.statics.getOne_advance = function(req, res, next, cb) {
    var newThisCollection = new this();
    var self = this;
    company.findOne({
        _id: req.user.selectedCompanyId
    }, function(err, company) {
        self.findOne({
                _id: company.siteId || null
            },
            function(err, site) {
                if (!site) {
                    createNewCollection(req, res, next, newThisCollection,
                        function() {
                            cb(newThisCollection);
                        }
                    );
                }
                else {
                    cb(site);
                };
            }
        );
    });
};


var createNewCollection = function(req, res, next, newThisCollection, cb) {
    newThisCollection.companyId = req.user.selectedCompanyId;
    newThisCollection.save(function(err) {
        company.findOne({
            _id: req.user.selectedCompanyId
        }, function(err, company) {
            company.siteId = newThisCollection.id;
            company.save(function(err) {
                cb(req, res, next, newThisCollection);
            })
        });
    });
}

module.exports = mongoose.model('Company_Site', CompanySiteSchema);