// This is a stack. any invoice record. There should be easy to seperate by type.
var mongoose = require('mongoose');



var InvoiceRecieptSchema = new mongoose.Schema({
    timestamp: {
        type: Date
    },
    invoiceId: {
        type: mongoose.Schema.Types.ObjectId
    },
    companyId: {
        type: mongoose.Schema.Types.ObjectId
    },
    payedBuyUserId: {
        type: mongoose.Schema.Types.ObjectId
    },
    payedBuyUserName: {
        type: String
    },
    recieptMessage: {
        type: String
    },
    stripeReturn: {
        type: mongoose.Schema.Types.Mixed
    }



});

InvoiceRecieptSchema.statics.SaveOne = function(invoiceReciept, cb){
    invoiceReciept.save(function(err){
        cb(err,invoiceReciept);
    });
};


InvoiceRecieptSchema.statics.getOne = function(invoiceIssueId, cb) {
    var tempObj = new this();
    this.findOne({
        _id: invoiceIssueId
    }, function(err, invoiceReciept) {
        if (invoiceReciept) {
            cb(err, invoiceReciept);
        }
        else {
            cb(err, tempObj);
        }

    })
};
InvoiceRecieptSchema.statics.saveOne = function(invoiceId, companyId, payedBuyUserId, payedBuyUserName, recieptMessage, cb) {
    var tempInvoiceIssue = new this();
    tempInvoiceIssue.invoiceId = invoiceId;
    tempInvoiceIssue.companyId = companyId;
    tempInvoiceIssue.invoiceId = payedBuyUserId;
    tempInvoiceIssue.invoiceId = payedBuyUserName;
    tempInvoiceIssue.invoiceId = recieptMessage;
    tempInvoiceIssue.timestamp = new Date();

    tempInvoiceIssue.save(function(err) {
        cb(err, tempInvoiceIssue)
    });
};


module.exports = mongoose.model('Invoice_Reciept', InvoiceRecieptSchema);