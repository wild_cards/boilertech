//This is a stack.. all users are in the stack. Think ledger

var mongoose = require('mongoose');
var Company = require("./Company");
var appSetting = require("./App_Setting");
var InvoiceReciept = require("./Invoice_Reciept");

var InvoiceSchema = new mongoose.Schema({

    //invoice statuses and updates
    invoiceStatus: {
        type: String
    },
    payedOnDate: {
        type: Date
    },
    stripeTransactionId: {
        type: String
    },
    invoiceClosingNote: {
        type: String
    },
    //invoice Changes
    change_log: [String],
    changes: {
        type: Number,
        default: 1
    },
    changeId: {
        type: Number,
        default: 1
    },

    //Additional Misc
    invoiceNotes: [{
        note: {
            type: String
        },
        timestamp: {
            type: Date
        }
    }],
    timestamp: {
        type: Date
    },



    invoiceIssueId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    invoiceIssue_allMessagesRead: {
        type: Boolean,
        default: true
    },


    assign_to_company_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    //panel 1// now panel 3
    //id is invoice number

    dueDate: {
        type: String
    },
    statement_date: {
        type: String
    },
    service_period_start: {
        type: String
    },
    service_period_end: {
        type: String
    },


    //panel 2
    // from app settings
    panel_2_company_url: {
        type: String
    },
    panel_2_company_address1: {
        type: String
    },
    panel_2_company_address2: {
        type: String
    },
    panel_2_company_city: {
        type: String
    },
    panel_2_company_state: {
        type: String
    },
    panel_2_company_zip: {
        type: String
    },

    //panel 3 //now panel 1
    // from company 
    panel_3_company_name: {
        type: String
    },
    panel_3_company_url: {
        type: String
    },
    panel_3_company_address1: {
        type: String
    },
    panel_3_company_address2: {
        type: String
    },
    panel_3_company_city: {
        type: String
    },
    panel_3_company_state: {
        type: String
    },
    panel_3_company_zip: {
        type: String
    },

    //panel 4
    panel_4_message: {
        type: String,
        default: "Do you see any issues on this invoice? Go to this link:"
    },
    panel_4_link: {
        type: String,
        default: "/app/invoice_issues"
    },


    //money
    taxTotal: {
        type: Number
    },
    total: {
        type: Number
    },
    subtotal: {
        type: Number
    },
    items: [{
        name: {
            type: String
        },
        price: {
            type: Number
        },
        quantity: {
            type: Number
        },
        total: {
            type: Number
        }
    }]
});

InvoiceSchema.statics.getOne = function(invoiceId, cb) {
    this.findOne({
        _id: invoiceId
    }, function(err, invoice) {
        cb(err, invoice);
    });
};

InvoiceSchema.statics.changeInvoice = function(req, cb) {
    appSetting.getOne(function(err, appSetting) {

        Company.findOne({
            _id: req.body.companyId
        }, function(err2, company) {
            mongoose.model('Invoice').findOne({
                _id: company.currentInvoiceId
            }, function(err1, invoice) {
                invoice.change_log.push(JSON.stringify(invoice));
                invoice.changes = invoice.changes++;
                invoice.changeId = invoice.changeId++;

                invoice.dueDate = req.body.invoice_dueDate;
                invoice.statement_date = req.body.invoice_statement_date;
                invoice.service_period_start = req.body.invoice_service_period_start;
                invoice.service_period_end = req.body.invoice_service_period_end;

                invoice.items = [];
                for (var i = 0; i < req.body.items.length; i++) {
                    var item = {
                        name: req.body.items[i][0],
                        price: parseFloat(req.body.items[i][1]),
                        quantity: parseFloat(req.body.items[i][2]),
                        total: parseFloat(req.body.items[i][1]) * parseFloat(req.body.items[i][2])
                    };

                    invoice.items.push(item);
                }

                //subtotal, tax, total
                var totalAmount = 0.0;
                for (var i = 0; i < invoice.items.length; i++) {
                    totalAmount += invoice.items[i].total;
                }

                invoice.subtotal = parseFloat(totalAmount);
                invoice.taxTotal = parseFloat((appSetting.tax_percent / 100.0)) * parseFloat(totalAmount);
                console.log(appSetting);
                invoice.total = parseFloat(invoice.subtotal) + parseFloat(invoice.taxTotal);

                invoice.save(function(err) {
                    cb(err);
                });


            });

        });
    });
}

InvoiceSchema.statics.closeInvoice = function(req, cb) {
    // req will have
    // invoice_status: "Paid", "Forgiven", "Problem"
    // invoice_action: "close", "close delete"
    // invoiceClosingNote : string
    // invoice_note: String
    // payedOnDate : String
    Company.findOne({
        _id: req.body.companyId
    }, function(err2, company) {
        mongoose.model('Invoice').findOne({
            _id: company.currentInvoiceId
        }, function(err1, invoice) {
            InvoiceReciept.getOne(null, function(err3, invoiceReciept) {

                var pastInvoices = {};
                if (invoice) {
                    invoice.invoiceClosingNote = req.body.invoiceClosingNote || null;
                    invoice.invoiceStatus = req.body.invoice_status;
                    company.currentInvoiceId = null;
                    if (req.body.invoice_status == 'Paid') {
                        invoice.stripeTransactionId = req.body.stripeTransactionId || null;
                        console.log(req.body.payedOnDate);
                        if (req.body.payedOnDate) {
                            invoice.payedOnDate = new Date(req.body.payedOnDate);
                        };
                    };
                };

                if (invoice && req.body.invoice_action == 'close') {
                    pastInvoices = {
                        service_period_start: invoice.service_period_start,
                        service_period_end: invoice.service_period_end,
                        invoiceStatus: req.body.invoice_status,
                        invoiceId: invoice.id
                    };
                };

                if (req.body.invoice_status == "Paid") {
                    pastInvoices.recieptId = invoiceReciept.id;
                    company.currentRecieptId = invoiceReciept.id
                    if (req.body.payedOnDate) {
                        pastInvoices.payedOnDate = new Date(req.body.payedOnDate);
                    };
                }

                if (req.body.invoice_action == 'close') {
                    company.pastInvoices.push(pastInvoices)
                }

                company.save(function(err6) {
                    if (req.body.invoice_status == "Paid") {

                        invoiceReciept.invoiceId = invoice.id;
                        invoiceReciept.companyId = company.id;
                        invoiceReciept.payedBuyUserId = req.user.id;
                        invoiceReciept.payedBuyUserName = req.user.profile.name;
                        invoiceReciept.recieptMessage = "Thank you for your payment";
                    }
                    if (invoice) {
                        invoice.save(function(err4) {

                            if (req.body.invoice_status == "Paid") {
                                invoiceReciept.save(function(err5) {
                                    cb(err3);
                                });
                            }
                            else {
                                cb(err3);
                            };
                        });
                    }
                    else {
                        cb(err6)
                    };
                });
            });
        });
    });
};

InvoiceSchema.statics.successfullyPayed = function(req, cb) {
    var self = new this();
    Company.findOne({
        _id: req.user.selectedCompanyId
    }, function(err2, company) {
        mongoose.model('Invoice').findOne({
            _id: company.currentInvoiceId
        }, function(err1, invoice) {
            InvoiceReciept.getOne(null, function(err3, invoiceReciept) {


                invoice.invoiceStatus = "Paid";
                invoice.payedOnDate = new Date();

                console.log(company);
                invoice.save(function(err4) {
                    invoiceReciept.invoiceId = invoice.id;
                    invoiceReciept.companyId = company.id;
                    invoiceReciept.payedBuyUserId = req.user.id;
                    invoiceReciept.payedBuyUserName = req.user.profile.name;
                    invoiceReciept.recieptMessage = "Thank you for your payment";

                    invoiceReciept.save(function(err5) {

                        company.currentInvoiceId = null;
                        company.currentRecieptId = invoiceReciept.id
                        company.pastInvoices.push({
                            service_period_start: invoice.service_period_start,
                            service_period_end: invoice.service_period_end,
                            invoiceStatus: "Paid",
                            payedOnDate: new Date(),
                            invoiceId: invoice.id,
                            recieptId: invoiceReciept.id
                        });
                        company.save(function(err6) {
                            console.log(err6);
                            cb(err3);

                        });
                    });
                });


            });
        });
    });
}

InvoiceSchema.statics.create = function(req, cb) {
    var tempInvoice = new this();
    appSetting.getOne(function(err1, appSetting) {
        Company.getOne_advance(req.body.companyId, function(err2, company) {

            //panel 1
            tempInvoice.dueDate = req.body.invoice_dueDate;
            tempInvoice.statement_date = req.body.invoice_statement_date;
            tempInvoice.service_period_start = req.body.invoice_service_period_start;
            tempInvoice.service_period_end = req.body.invoice_service_period_end;

            //panel 2
            tempInvoice.panel_2_company_url = appSetting.app_url;
            tempInvoice.panel_2_company_address1 = appSetting.address_line1;
            tempInvoice.panel_2_company_address2 = appSetting.address_line2;
            tempInvoice.panel_2_company_city = appSetting.city;
            tempInvoice.panel_2_company_state = appSetting.state;
            tempInvoice.panel_2_company_zip = appSetting.zip;

            //panel 3
            tempInvoice.panel_3_company_name = company.company_name;
            tempInvoice.panel_3_company_url = company.company_url;
            tempInvoice.panel_3_company_address1 = company.company_address1;
            tempInvoice.panel_3_company_address2 = company.company_address2;
            tempInvoice.panel_3_company_city = company.company_city;
            tempInvoice.panel_3_company_state = company.company_state;
            tempInvoice.panel_3_company_zip = company.company_zip;

            //panel 4
            tempInvoice.panel_4_message = "Do you see any issues on this invoice? Go to this link:";
            tempInvoice.panel_4_link = "/app/invoice_issues";

            //panel items
            for (var i = 0; i < req.body.items.length; i++) {
                var item = {
                    name: req.body.items[i][0],
                    price: parseFloat(req.body.items[i][1]),
                    quantity: parseFloat(req.body.items[i][2]),
                    total: parseFloat(req.body.items[i][1]) * parseFloat(req.body.items[i][2])
                };

                tempInvoice.items.push(item);
            }

            //subtotal, tax, total
            var totalAmount = 0.0;
            for (var i = 0; i < tempInvoice.items.length; i++) {
                totalAmount += tempInvoice.items[i].total;
            }

            tempInvoice.subtotal = parseFloat(totalAmount);
            tempInvoice.taxTotal = parseFloat((appSetting.tax_percent / 100.0)) * parseFloat(totalAmount);
            tempInvoice.total = parseFloat(tempInvoice.subtotal) + parseFloat(tempInvoice.taxTotal);

            tempInvoice.save(function(err) {
                Company.findOne({
                    _id: req.body.companyId
                }, function(err, company) {
                    company.currentInvoiceId = tempInvoice.id;
                    company.save(function(err) {
                        cb(err);
                    })
                })
            });
        });
    });
}

module.exports = mongoose.model('Invoice', InvoiceSchema);