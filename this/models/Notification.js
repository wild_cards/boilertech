var mongoose = require('mongoose');
var moment = require('moment');

var notificationSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  notifications: [{
    timestamp: {
      type: Date,
      default: Date.now
    },
    isRead: {
      type: Boolean,
      default: false
    },
    message: String,
    type: {
      type: String,
      default: ''
    },
    url: {
      type: String,
      default: ''
    },
    urlName: {
      type: String,
      default: ''
    }
  }],
});

/**
 * Save a notification then emit it to user.
 * Example: require('../models/Notification').sendNotification(io, req.user.id, 'This is an example notification.', 'reminder', '/app/gohere');
 * Use Case: Pass in the io object, user's id and a message from any controller to send a notification after an action.
 */
notificationSchema.statics.sendNotification = function(id, message, path, type) {  //io
  if (id == null) {
    return false;
  }

  this.findOne({
    user: id
  }).exec(function(err, result) {
    if (result == null) {
      result = mongoose.model('Notification', notificationSchema)({
        user: id
      });
    }

    result.notifications.push({
      message: message,
      type: type,
      url: path
    });

    result.save(function(err) {
      if (err) {
        throw err;
      }

      // result.unread = 0;

      // result.notifications.forEach(function(element, index, array) {
      //   result.notifications[index].date = moment(element.timestamp).format('MM/DD/YYYY hh:mm:ss a');
      //   if (element.isRead == false) {
      //     result.unread++;
      //   }
      // });

      // var notification = result.notifications.pop();

      // io.sockets.in(id).emit('notification', {
      //   date: notification.date,
      //   isRead: notification.isRead,
      //   message: notification.message,
      //   type: notification.type,
      //   unread: result.unread,
      //   _id: notification.id
      // });
    });
  });
};

notificationSchema.statics.getNotifications = function(id, cb, getAll) {
  this.findOne({
    user: id
  }).exec(function(err, result) {
    if (result == null || result.notifications.length == 0) {
      result = {};
      result.success = false;
    }
    else {
      result.success = true;
      result.unread = 0;
      var notifications = [];

      result.notifications.forEach(function(element, index, array) {
        result.notifications[index].date = result.notifications[index]._doc.date = moment(element.timestamp).format('MM/DD/YYYY hh:mm:ss a');
        if (!element.isRead) {
          result.unread++;
        }

        if (getAll) {
          notifications.push(element);
        }
        else {
          if (!element.isRead) {
            notifications.push(element);
          }
        }
      });
    }

    result.notifications = notifications;

    cb(result);
  });
};

notificationSchema.statics.readNotification = function(id, nid, cb) {
  this.findOne({
    user: id
  }).exec(function(err, result) {
    if (!result || nid == undefined) {
      result.success = false;
      cb(result);
    }
    else {
      result.unread = 0;

      result.notifications.forEach(function(element, index, array) {
        if (nid == element.id) {
          result.notifications[index].isRead = true;
          result.url = element.url;
        }

        if (result.notifications[index].isRead == false) {
          result.unread++;
        }
      });

      result.save(function(err) {
        if (err) {
          throw err;
        }

        result.success = true;

        cb(result);
      });
    }
  });
};

notificationSchema.statics.notificationIsRead = function(id, nid, cb) {
  this.findOne({
    user: id
  }).exec(function(err, result) {
    if (!result || nid == undefined) {
      result.success = false;
      cb(result);
    }
    else {
      result.notifications.forEach(function(element, index, array) {
        if (nid == element.id) {
          result.notifications[index].isRead = true;
        }
      });

      result.save(function(err) {
        if (err) {
          throw err;
        }

        result.success = true;

        cb(result);
      });
    }
  });
};

module.exports = mongoose.model('Notification', notificationSchema);