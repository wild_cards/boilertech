var mongoose = require('mongoose');
var company = require("./Company");

var CompanySiteLeadSchema = new mongoose.Schema({
  companyId: {
    type: mongoose.Schema.Types.ObjectId
  },

  leads: [{
    isRead:{
      type: Boolean,
      default: false
    },
    message: {
      type: String
    },
    requestAppointment: {
      type: Boolean,
      default: false
    },
    customer: {
      firstName: {
        type: String
      },
      lastName: {
        type: String
      },
      email: {
        type: String
      },
      phoneNumber: {
        type: String
      }
    },

    appointment: {
      firstChoice: {
        type: String
      },
      secondChoice: {
        type: String
      },
      thirdChoice: {
        type: String
      }
    }
  }]

});

CompanySiteLeadSchema.statics.getOne = function(req, res, next, cb) {
  var newInstanceCollection = new this();
  this.findOne({
    companyId: req.body.companyId
  }, function(err, companySiteLead) {
    if (err || !req.body.companyId) {
      return next();
    }

    if (!companySiteLead) {
      newInstanceCollection.companyId = req.body.companyId;
      newInstanceCollection.save(function(err) {
        cb(newInstanceCollection);
      })
    }
    else {
      cb(companySiteLead);
    }

  });
}

CompanySiteLeadSchema.statics.getOne_advance = function(req, res, next, cb) {
  var newInstanceCollection = new this();
  this.findOne({
    companyId: req.user.selectedCompanyId
  }, function(err, companySiteLead) {
    if (err || !req.user.selectedCompanyId) {
      return next();
    }

    if (!companySiteLead) {
      newInstanceCollection.companyId = req.user.selectedCompanyId;
      newInstanceCollection.save(function(err) {
        cb(newInstanceCollection);
      })
    }
    else {
      cb(companySiteLead);
    }

  });
}

module.exports = mongoose.model('Company_Site_Lead', CompanySiteLeadSchema);