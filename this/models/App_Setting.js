var mongoose = require('mongoose');
var appSettingSchema = new mongoose.Schema({
  app_url: {
    type: String,
    default: 'examplesite.com'
  },
  app_anchor_url:{
    type: String
  },
  address_line1: {
    type: String,
    default: 'P.O. Box 123'
  },
  address_line2: {
    type: String,
    default: ''
  },
  city: {
    type: String,
    default: "Arcadia"
  },
  state: {
    type: String,
    default: "CA"
  },
  zip: {
    type: String,
    default: "91007"
  },
  phone: {
    type: String,
    default: "(234) 123-2345"
  },
  email: {
    type: String,
    default: "example@example.com"
  },
  tax_percent: {
    type: Number,
    default: 10.5
  },
  late_payment: {
    type: Number,
    default: 0
  },
  companies_invoice_send_date: {
    type: Number,
    default: 15
  },
  companies_invoice_process_date: {
    type: Number,
    default: 1
  }
});


appSettingSchema.statics.getOne = function(cb) {
  // if (req.user.role != 'webmaster') {
  //   return;
  // }

  var newRecord = new this();
  this.findOne(function(err, record) {
    if (!record) {
      newRecord.save(function(err) {

        cb(err, newRecord);
      });
    }
    else {
      cb(err, record);
    }
  })
}

appSettingSchema.statics.saveRecord = function(req, cb) {
  this.findOne(function(err, record) {
    record.app_url = req.body.app_url;
    record.app_anchor_url = req.body.app_anchor_url;
    record.address_line1 = req.body.address_line1;
    record.address_line2 = req.body.address_line2;
    record.city = req.body.city;
    record.state = req.body.state;
    record.zip = req.body.zip;
    record.phone = req.body.phone;
    record.email = req.body.email;
    record.tax_percent = req.body.tax_percent;
    record.late_payment = req.body.late_payment;
    record.companies_invoice_send_date = req.body.companies_invoice_send_date;
    record.companies_invoice_process_date = req.body.companies_invoice_process_date;
    
    record.save(function(err) {
      cb(err);
    })
  })

};


module.exports = mongoose.model('App_Setting', appSettingSchema);
