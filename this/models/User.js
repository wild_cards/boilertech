var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true
  },
  password: String,

  facebook: String,
  twitter: String,
  google: String,
  github: String,
  instagram: String,
  linkedin: String,
  steam: String,
  tokens: Array,

  isUserDisabled: {
    type: Boolean,
    default: false
  },
  isLockedOut: {
    type: Boolean,
    default: false
  },
  role: {
    type: String,
    lowercase: true
  },
  keys: [{
    type: String,
    unique: true,
    lowercase: true
  }],

  selectedCompanyId: mongoose.Schema.Types.ObjectId,
  //remove or add links
  userSiteAccess: {
    powerUser:{
      type: Boolean,
      default: false
    },
    companyAdmin:{ //companyInfo, manageUsers, paymentCenter,
      type: Boolean,
      default: false
    },
    dashboard: {
      type: Boolean,
      default: true
    },
    manageSite: {
      type: Boolean
      //this is probably going to change. should be true
    },
    analytics: {
      type: Boolean,
      default: true
    },
    marketing: {
      type: Boolean,
      default: false
    }

  },

  profile: {
    name: {
      type: String,
      default: ''
    },
    gender: {
      type: String,
      default: ''
    },
    location: {
      type: String,
      default: ''
    },
    website: {
      type: String,
      default: ''
    },
    picture: {
      type: String,
      default: ''
    }
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date
});

/**
 * Password hash middleware.
 */
userSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(10, function(err, salt) {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

/**
 * Helper method for getting user's gravatar.
 */
userSchema.methods.gravatar = function(size) {
  if (!size) {
    size = 200;
  }
  if (!this.email) {
    return 'https://gravatar.com/avatar/?s=' + size + '&d=retro';
  }
  var md5 = crypto.createHash('md5').update(this.email).digest('hex');
  return 'https://gravatar.com/avatar/' + md5 + '?s=' + size + '&d=retro';
};

userSchema.statics.getBySelectedCompany = function(selectedCompanyId, cb){
  this.find({selectedCompanyId: selectedCompanyId}, function(err, users){
    cb(err, users);
  });
};

userSchema.statics.getUserWithCompany = function(companyId, userId, cb){
  this.findOne({$and:{ _id: userId, selectedCompanyId : companyId }}, function(err, user) {
    cb(err, user);
  })
}

module.exports = mongoose.model('User', userSchema);
