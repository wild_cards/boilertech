var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');

var fileUploadSchema = new mongoose.Schema({
  originalname: {
    type: String,
    default: null
  },
  encoding: {
    type: String,
    default: false
  },
  mimetype: {
    type: String,
    default: null
  },
  buffer:{
    type: String,
    default: null
  },
  destination: {
    type: String,
    default: null
  },
  filename: {
    type: String,
    default: null
  },
  path: {
    type: String,
    default: null
  },
  size: {
    type: String,
    default: null
  }
});

fileUploadSchema.plugin(timestamps);

fileUploadSchema.statics.getOne = function(req, cb){
    this.findOne({_id: req.query.id.split('.')[0] }, function(err, fileUpload){
        console.log(fileUpload);
        return cb(err, fileUpload);
        
    })
};

fileUploadSchema.statics.saveOne = function(req, cb){
    console.log(req.file);
    if (req.file || req.files){
        
    } else {
        return cb(null);
    }
    var tempFileUploadDoc = new this();
    tempFileUploadDoc.fieldname = req.file.fieldname;
    tempFileUploadDoc.originalname = req.file.originalname;
    tempFileUploadDoc.encoding = req.file.encoding;
    tempFileUploadDoc.mimetype = req.file.mimetype;
    tempFileUploadDoc.destination = req.file.destination;
    tempFileUploadDoc.filename = req.file.filename;
    tempFileUploadDoc.path = req.file.path;
    tempFileUploadDoc.size = req.file.size;
    tempFileUploadDoc.buffer = req.file.buffer.toString();
    tempFileUploadDoc.save(function(err){
       cb(err,tempFileUploadDoc); 
    });
};



module.exports = mongoose.model('File_Upload', fileUploadSchema);
