var goToLink = function(notificationId, element) {
   $(element).parent().attr('style', 'color:green').html('Read');
   $.ajax({
      type: "POST",
      url: "/app/notificationIsRead",
      data: {
         id: $('#uid').val(),
         nid: notificationId,
         _csrf: $('#csrf').val()
      }
   });

   
}

var notificationLink = function(notificationId, notificationUrl, notificationIsRead) {
      if(!notificationIsRead){
         $.ajax({
            type: "POST",
            url: "/app/notificationIsRead",
            data: {
               id: $('#uid').val(),
               nid: notificationId,
               _csrf: $('#csrf').val()
            }
         });
      }
      
      window.location = notificationUrl;
   }