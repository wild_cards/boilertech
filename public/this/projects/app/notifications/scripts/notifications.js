$(function() {
    var notificationBox = $('.notification');
    
    function AddNotification(notification) {
        notificationBox.prepend('<li id=' + notification._id + '><a><strong>' + notification.message + ' </strong><br/>' + notification.date + '</a></li>');

        if (notificationBox.find('li').length > 5) {
            notificationBox.addClass('scrollable-notification');
        } else {
            notificationBox.removeClass('scrollable-notification');
        }
    };

    function UpdateUnread(unread) {
        if (unread == 0) {
            $('.new-notifications').addClass('hidden')
        }
        else {
            $('.new-notifications').removeClass('hidden');
        }
        if(unread > 10){
            unread = '9+';
        }
        $('.new-notifications').text('' + unread);
    };

    function MarkAsRead(notification) {
        notification.find('span').remove();
    };
    
    function ReadNotification(element) {
        var nid = element.attr('id');

        if (nid) {
            $.ajax({
                url: '/app/readnotification',
                method: 'POST',
                data: {
                    nid: nid,
                    _csrf: $('#csrf').val()
                }
            }).done(function(result) {
                if (result.success) {
                    MarkAsRead(element);
                    UpdateUnread(result.unread);
                    if (result.url.length) {
                        window.location = result.url;
                    }
                }
            });
        }
    }

    /* Get existing notifications */
    $.ajax({
        url: '/app/notifications'
    }).done(function(result) {
        if (result.success) {
            result.notifications.forEach(function(notification) {
                AddNotification(notification);
            });
            UpdateUnread(result.unread);
        }
    });

    /* Listen for new notification pushes */
    // var socket = io.connect();

    // socket.on('connect', function() {
    //     socket.emit('join', {
    //         uid: $('#uid').val()
    //     });

    //     socket.on('notification', function(notification) {
    //         AddNotification(notification);
    //         UpdateUnread(notification.unread);
    //     });
    // });

    /* Notification events */
    // Dropdown notification click
    $('.notification').on('click', 'li', function() {
        ReadNotification($(this));
    });
    
    // Full notification click
    $('.read-notification').click(function() {
        ReadNotification($(this));
    });

    // Create new example notification
    $('.test-notification').click(function() {
        $.ajax({
            url: '/app/sendnotification',
            method: 'POST',
            data: {
                _csrf: $('#csrf').val()
            }
        });
    });
});
