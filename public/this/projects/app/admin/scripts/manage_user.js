var powerUserToggle = function(){
    if($('#powerUserCheckbox').is(":checked")){
        $('#alertPower').show();
        $('#advanceOptions').hide();
    } else {
        $('#alertPower').hide();
        $('#advanceOptions').show();
    }
}

powerUserToggle();