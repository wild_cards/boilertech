var ViewModel = function(bioObj){
    var self = this;
    console.log(bioObj);
    self.paragraphs = ko.observableArray(bioObj.paragraphs);
    self.bio = ko.observable(bioObj);
    
    self.addParagraph = function() {
        self.paragraphs.push({ line: "" });
    };
 
    self.removeParagraph = function() {
        self.paragraphs.remove(this);
    };
    
    self.init = function(){
        if(self.paragraphs() < 1){
            self.addParagraph();
        }
    };
    
    self.init();
};