var ViewModel = function(servicesObj){
    var self = this;
    console.log(servicesObj);
    self.services = ko.observableArray(servicesObj);
    
    self.addService = function() {
        self.services.push({ name: "", description: "" });
    };
 
    self.removeService = function() {
        self.services.remove(this);
    };
    
    self.init = function(){
        if(self.services() < 1){
            self.addService();
        }
    };
    
    self.init();
};