 $(function() {

     $('.demo1').click(function() {
         swal({
             title: "Welcome in Alerts",
             text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
         });
     });

     $('.demo2').click(function() {
         swal({
             title: "Good job!",
             text: "You clicked the button!",
             type: "success"
         });
     });

     $('.demo3').click(function() {
         swal({
                 title: "Are you sure?",
                 text: "Your will not be able to recover this imaginary file!",
                 type: "warning",
                 showCancelButton: true,
                 confirmButtonColor: "#DD6B55",
                 confirmButtonText: "Yes, delete it!"
             },
             function() {
                 swal("Booyah!");
             });
     });

     $('.demo4').click(function() {
         swal({
                 title: "Are you sure?",
                 text: "Your will not be able to recover this imaginary file!",
                 type: "warning",
                 showCancelButton: true,
                 confirmButtonColor: "#DD6B55",
                 confirmButtonText: "Yes, delete it!",
                 cancelButtonText: "No, cancel plx!",
                 closeOnConfirm: false,
                 closeOnCancel: false
             },
             function(isConfirm) {
                 if (isConfirm) {
                     swal("Deleted!", "Your imaginary file has been deleted.", "success");
                 }
                 else {
                     swal("Cancelled", "Your imaginary file is safe :)", "error");
                 }
             });
     });

     // Toastr options
     toastr.options = {
         "debug": false,
         "newestOnTop": false,
         "positionClass": "toast-top-center",
         "closeButton": true,
         "debug": false,
         "toastClass": "animated fadeInDown",
     };

     $('.homerDemo1').click(function() {
         toastr.info('Info - This is a custom Homer info notification');
     });

     $('.homerDemo2').click(function() {
         toastr.success('Success - This is a Homer success notification');
     });

     $('.homerDemo3').click(function() {
         toastr.warning('Warning - This is a Homer warning notification');
     });

     $('.homerDemo4').click(function() {
         toastr.error('Error - This is a Homer error notification');
     });

 });