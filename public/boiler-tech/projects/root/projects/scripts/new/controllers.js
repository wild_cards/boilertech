'use strict';
myApp

.controller("main", function($scope, $uibModal, $rootScope, dataFactory){
  //Globals
  $rootScope.pages = [];
  $rootScope.selectedNumber = 0;
  //End
  $scope.projectName = "";
  $scope.projectNameDisabled = false;
  $scope.projectPurpose = "";
  $scope.projectPurposeDisabled = false;
  
  $scope.deletePageInput = '';
  
  $scope.response = "Page";
  $scope.verb = "Get";
  
  $scope.keyChain = [];
  $scope.postPageKeySelect = {};
  $scope.getPageKeySelect = {};
  $scope.postDataKeySelect = {};
  $scope.getDataKeySelect = {};

  dataFactory.getKeyChain().then(function(res){
    $scope.keyChain = res.data; 
    $scope.postPageKeySelect = $scope.keyChain[0];
    $scope.getPageKeySelect = $scope.keyChain[0];
    $scope.postDataKeySelect = $scope.keyChain[0];
    $scope.getDataKeySelect = $scope.keyChain[0];
  });
  
  $scope.AddCSSFile = function(){
    $rootScope.pages[$rootScope.selectedNumber].getPage.cssFiles.push("");
  }
  
  $scope.removeCSSFile = function(index){
    $rootScope.pages[$rootScope.selectedNumber].getPage.cssFiles.splice(index, 1);
  }
  
  $scope.AddJSFile = function(){
    $rootScope.pages[$rootScope.selectedNumber].getPage.jsFiles.push("");
  }
  
  $scope.removeJSFile = function(index){
    $rootScope.pages[$rootScope.selectedNumber].getPage.jsFiles.splice(index, 1);
  }
  
  $scope.postPageKeyAdd = function(index){
    var userKeys = $rootScope.pages[$rootScope.selectedNumber].postPage[index].keys;
    
    if(userKeys.length > 0){
      for (var i = 0; i < userKeys.length; i++) {
        if(userKeys[i].name == $scope.postPageKeySelect.name){
          return;
        }
      }  
    } 
    
    $rootScope.pages[$rootScope.selectedNumber].postPage[index].keys.push($scope.postPageKeySelect);
  };
  
  $scope.getPageKeyAdd = function(){
    var userKeys = $rootScope.pages[$rootScope.selectedNumber].getPage.keys;
    
    if(userKeys.length > 0){
      for (var i = 0; i < userKeys.length; i++) {
        if(userKeys[i].name == $scope.getPageKeySelect){
          return;
        }
      }  
    } 
    
    $rootScope.pages[$rootScope.selectedNumber].getPage.keys.push($scope.getPageKeySelect);
    
  };
  
  $scope.postDataKeyAdd = function(index){
    var userKeys = $rootScope.pages[$rootScope.selectedNumber].postData[index].keys;
    
    if(userKeys.length > 0){
      for (var i = 0; i < userKeys.length; i++) {
        if(userKeys[i].name == $scope.postDataKeySelect){
          return;
        }
      }  
    } 
    
    $rootScope.pages[$rootScope.selectedNumber].postData[index].keys.push($scope.postDataKeySelect);
    
  };
  
  $scope.getDataKeyAdd = function(){
    var userKeys = $rootScope.pages[$rootScope.selectedNumber].getData.keys;
    
    if(userKeys.length > 0){
      for (var i = 0; i < userKeys.length; i++) {
        if(userKeys[i].name == $scope.getDataKeySelect){
          return;
        }
      }  
    } 
    
    $rootScope.pages[$rootScope.selectedNumber].getData.keys.push($scope.getDataKeySelect);
    
  };
  
  $scope.postPageKeyRemove = function(pageIndex, keyIndex){
    $rootScope.pages[$rootScope.selectedNumber].postPage[pageIndex].keys.splice(keyIndex, 1);
  }
  
  $scope.getPageKeyRemove = function(keyIndex){
    $rootScope.pages[$rootScope.selectedNumber].getPage.keys.splice(keyIndex, 1);
  }
  
  $scope.postDataKeyRemove = function(pageIndex, keyIndex){
    $rootScope.pages[$rootScope.selectedNumber].postData[pageIndex].keys.splice(keyIndex, 1);
  }
  
  $scope.getDataKeyRemove = function(keyIndex){
    $rootScope.pages[$rootScope.selectedNumber].getData.keys.splice(keyIndex, 1);
  }
  
  
  $scope.PageName = function(){
    if($rootScope.pages.length > 0) {
      return $rootScope.pages[$rootScope.selectedNumber].pageName;
    } else {
      return "";
    }
  };
  
  $scope.PagePurpose = function(){
      return $rootScope.pages[$rootScope.selectedNumber].pagePurpose;

  };
  
  $scope.changeSelectedNumber = function(num){
    $rootScope.selectedNumber = num;
  };
  
  $scope.removeGetPage = function(){
    $rootScope.pages[$rootScope.selectedNumber].isGetPage = false;
  };
  
  $scope.removeGetData = function(){
    $rootScope.pages[$rootScope.selectedNumber].isGetData = false;
  };
  
  $scope.removePostData = function(index){
    $rootScope.pages[$rootScope.selectedNumber].postData.splice(index, 1);
  };
  
  $scope.removePostPage = function(index){
    $rootScope.pages[$rootScope.selectedNumber].postPage.splice(index, 1);
  };
  
  $scope.scaffoldingTile = function(){
    var mainObj = $rootScope.pages[$rootScope.selectedNumber];
    
    
    switch ($scope.response) {
      case 'Page':
        if($scope.verb == "Get"){
          mainObj.isGetPage = true;
        }else if($scope.verb == "Post"){
          mainObj.postPage.push(new mainObj.postPageObj($scope.projectName, $scope.PageName(), $scope.keyChain));
        }
        
        break;
      case 'Data':
        if($scope.verb == "Get"){
          $rootScope.pages[$rootScope.selectedNumber].isGetData = true;
        }else if($scope.verb == "Post"){
          mainObj.postData.push(new mainObj.postDataObj($scope.projectName, $scope.PageName(), $scope.keyChain));
        }
        break;
    }
    
    // $rootScope.pages[$rootScope.selectedNumber].
  };
  
  
  $scope.deleteModal = function(){
    $uibModal.open({
      animation: true,
      templateUrl: 'DeleteModal.html',
      controller: 'deleteProjectCtrl',
      size: "small",
      windowClass: "hmodal-danger"
    });
  }
  
  
  
  $scope.createModal = function(){
    if($scope.projectName == '' || $scope.projectPurpose == ''){
      
      $uibModal.open({
        animation: true,
        templateUrl: 'createProjectNoInfoError.html',
        controller: 'createProjectCtrl',
        size: "small",
        windowClass: "hmodal-danger"
      });
      
      return;
    }
  
    //disable Project Name and Project Purpose
    $scope.projectNameDisabled = true;
    $scope.projectPurposeDisabled = true;
    
    $uibModal.open({
      animation: true,
      templateUrl: 'createProjectModal.html',
      controller: 'createProjectCtrl',
      size: "small",
      windowClass: "hmodal-success",
      resolve:{
          projectName: function(){
            return $scope.projectName;
          } 
      }
    });
  };
  
  $scope.submit = function(){
    
  }
  
})
  .controller("createProjectCtrl", function ($scope, $uibModalInstance, $rootScope,projectName) {
  
    $scope.ok = function () {
      if($scope.newPageInput == ''){
        $scope.newPageInputError = "Please enter a name for the page.";
        return;
      } else {
        $rootScope.pages.push(new formObj(projectName, $scope.newPageInput));
        $rootScope.selectedNumber = $rootScope.pages.length - 1;
      }
      
      $uibModalInstance.close();
    };
  
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  })

  .controller("deleteProjectCtrl", function ($scope, $uibModalInstance, $rootScope) {
    $scope.delete = function () {
      if($scope.deletePageInput != $rootScope.pages[$rootScope.selectedNumber].pageName){
        return;
      };
      
      $rootScope.pages.splice($rootScope.selectedNumber, 1);
      $rootScope.selectedNumber = $rootScope.pages.length - 1;
      $uibModalInstance.close();
    };
  
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });

// angular.module('myApp').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance) {

//   $scope.ok = function () {
//     $uibModalInstance.close();
//   };

//   $scope.cancel = function () {
//     $uibModalInstance.dismiss('cancel');
//   };
// });