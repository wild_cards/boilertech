app.controller('main', function($scope, $rootScope, dataService, $uibModal, $log) {
    $scope.allUsers = [];
    $scope.userSelected;
    $scope.selectedNumber = 0;
    $scope.allUsersSelected = [];
    dataService.getUsers().success(function(data) {
        $scope.allUsers = data;
        if (data.length > 0) {
            $scope.userSelected = $scope.allUsers[0];
        }
    });

    $scope.addUser = function() {
        var userNotAdded = false;
        for (var i = 0; i < $scope.allUsersSelected.length; i++) {
            if ($scope.allUsersSelected[i] == $scope.userSelected) {
                return;
            }
        }
        $scope.allUsersSelected.push($scope.userSelected);
        $scope.selectedNumber = $scope.allUsersSelected.length - 1;
    }

    $scope.open = function(size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: 'ModalCreateInvoiceCtrl',
            size: "lg",
            windowTopClass: "hgreen"
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem;
        }, function() {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

});

app.controller('ModalCreateInvoiceCtrl', function($scope, $uibModalInstance) {
    $scope.ok = function() {
        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
