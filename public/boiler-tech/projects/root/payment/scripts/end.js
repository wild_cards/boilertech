$(function() {
    invoiceStatusChange();
    $('#datepicker').datepicker();
    $("#datepicker").on("changeDate", function(event) {
        $("#my_hidden_input").val(
            $("#datepicker").datepicker('getFormattedDate')
        )
    });

    $('#datapicker2').datepicker();
    $('.input-group.date').datepicker({});
    $('.input-daterange').datepicker({});
    
});


var load = function(){
  invoiceStatusChange();  
}

var invoiceStatusChange = function(){
    var selected = $('#invoiceStatisSelect').val();
    switch (selected) {
        case 'Forgiven':
            $('#paidBox').hide();
            break;
        case 'Problem':
            $('#paidBox').hide();
            break;
        case 'Paid':
            $('#paidBox').show();
            break;
    }
};