var viewsObj = function(name, id, projectName, listOfKeyes) {
  var self = this;
  self.name = name;
  self.id = id;
  self.projectName = projectName;

  self.addTag = function(selector) {
    var pageName = self.name,
      id = self.id,
      projectName = self.projectName,
      selector = selector || '#tabs>ul';

    $(selector).append('
<li id="tab' + id + '">
    <a data-toggle="tab" href="#tab-' + id + '" aria-expanded="false">
        <strong>' + pageName + ' &nbsp; &nbsp;</strong>
        <button type="button" class="btn btn-danger" onclick="app.deleteModal(' + id + ')">
            <strong>
                <i class="pe-7s-close fa-2x"></i>
            </strong>
        </button>
    </a>
</li>
')
  };

  self.addPermissions = function(permissionName, permissionId){
    return '
    <div id=' + self.name + permissionId + ' class="row well">
      <div class="col-sm-8">
        ' + permissionName + '
      </div>
      <div class="col-md-1 pull-right btn btn-danger" onclick="app.deletePermissionModal('' + self.name + '','' + permissionName + '', '' + permissionId + '')">
        <input type="hidden" value="' + permissionId + '" />
        <strong>X</strong>
        
      </div>
    </div>
    ';
  };

  self.addKeyes = function(elementId){
    var keyesHtml = $('#Keys').html();
    keyesHtml = keyesHtml.replace('tempIdHolder',elementId);
    
    return keyesHtml;
  };

  self.addFormBody = function(selector) {
    var name = self.name,
      id = self.id,
      projectName = self.projectName,
      selector = selector || '#formBody';

    String.prototype.capitalizeFirstLetter = function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    }

    $(selector).append('
<div  class="tab-pane">
    <br/>
  <div id="main" class="row">
    <div class="form-group">
      <label for="scaffoldingName" class="col-sm-3 control-label">Page Name</label>
      <div class="col-sm-7">
        <input id="scaffoldingName" name="scaffoldingName" disabled="disabled" class="form-control"/>
      </div>
    </div>
    <div class="form-group">
      <label for="projectPurpose" class="col-sm-3 control-label">Page Purpose</label>
      <div class="col-sm-7">
        <input id="scaffoldingPurpose" class="form-control"/>
      </div>
    </div>
    <div class="form-group">
      <label for="url" class="col-sm-3 control-label">Url</label>
      <div class="col-sm-7">
        <input id="url" class="form-control" />
      </div>
    </div>
    <div class="form-group">
      <label for="isOAuth" class="col-sm-3 control-label">OAuth</label>
      <div class="col-sm-7">
        <input type="checkbox" checked="true" class="form-inline pull-left"/>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="hr-line-dashed"></div>
    <div class="form-group">
      <label for="roles" class="col-sm-4 control-label">Roles</label>
      <div class="col-sm-7">
        <select name="roles" id="roles" class="form-control m-b">
          <option>Webmaster</option>
          <option>Admin</option>
          <option>Manager</option>
          <option>Worker</option>
          <option>User</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Keys</label>
      <div class="col-sm-6">
      </div>
      <div class="col-sm-2">
        <div class="btn btn-primary">
          <i class="pe-7s-plus"></i>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label"></label>
      <div class="col-sm-7">
      
      </div>
    </div>
  </div>
  <div class="hpanel hgreen">
    <div class="panel-heading">
        Scaffolding
      </div>
      <div class="panel-body">

      <div class="form-group">
        <label for="ResponseOption" class="col-sm-3 control-label">Response</label>
        <div class="col-sm-7">
          <select name="ResponseOption" id="ResponseOption" class="form-control m-b">
            <option>Page</option>
            <option>Data</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="VerbOption" class="col-sm-3 control-label">Verb</label>
        <div class="col-sm-7">
          <select name="VerbOption" id="VerbOption" class="form-control m-b">
            <option>Get</option>
            <option>Post</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-7">
          <div class="btn btn-primary">
            Generate Scaffolding Tile
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="hr-line-dashed"></div>
  <div>
    <div class="form-group">
      <label for="jadeName" class="col-sm-4 control-label">View File</label>
      <div class="col-sm-7">
        <div class="col-sm-5">
          <input type="text" disabled="true" class="form-control pull-left"/>
        </div>
        <div class="col-sm-5">
          <input type="text" id="jadeName" class="form-control"/>
        </div>
        <div class="col-sm-2">
          <input type="text" value=".jade" disabled="true" class="form-control"/>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="controllerName" class="col-sm-4 control-label">Choose layout</label>
      <div class="col-sm-7">
        <select name="roles" id="roles" class="form-control m-b">
          <option>Default</option>
          <option>Empty</option>
          <option>Exampleapp/Alerts</option>
          <option>Exampleapp/Blah</option>
          <option>Exampleapp/Text</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="controllerName" class="col-sm-4 control-label">Include Files</label>
      <div class="col-sm-7">
        <div class="row">
          <input type="checkbox" checked="true" class="form-inline"/>.css
        </div>
        <div class="row">
          <input type="checkbox" checked="true" class="form-inline"/>.js
        </div>
      </div>
    </div>
    <div>
      <div class="form-group">
        <label for="jadeName" class="col-sm-4 control-label">Css File</label>
        <div class="col-sm-7">
          <div class="col-sm-5">
            <input type="text" disabled="true" class="form-control"/>
          </div>
          <div class="col-sm-5">
            <input type="text" id="jadeName" class="form-control"/>
          </div>
          <div class="col-sm-2">
            <input type="text" value=".css" disabled="true" class="form-control"/>
          </div>
        </div>
      </div>
    </div>
    <div>
      <div class="form-group">
        <label for="jadeName" class="col-sm-4 control-label">JS File</label>
        <div class="col-sm-7">
          <div class="col-sm-5">
            <input type="text" disabled="true" class="form-control"/>
          </div>
          <div class="col-sm-5">
            <input type="text" id="jadeName" class="form-control"/>
          </div>
          <div class="col-sm-2">
            <input type="text" value=".js" disabled="true" class="form-control"/>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="form-group">
      <label for="jadeName" class="col-sm-4 control-label">Redirect</label>
      <div class="col-sm-7">
        <input type="text" placeholder="/app/example" class="form-control"/>
      </div>
    </div>
  </div>
</div>
    ')
  };

};


// <div class="form-group">
//   <label for="controllerActionName" class="col-sm-3 control-label">Controller Action Name</label>
//   <div class="col-sm-7">
//     <input type="text" id="controllerActionName" class="form-control pull-left" value="get' + name.capitalizeFirstLetter() + '"/>
//   </div>
// </div>


//Add url with oath for post

//only one get